package com.example.myapp

import java.time.LocalDate
import java.time.LocalDateTime

data class Task(
    val taskNumber: Int,
    val name: String,
    val description: String = "",
    val creator: User,
    val priority: Int = 0,
    val executor: User? = null,
    var apprasial: Double = 0.0,
    val status: String = "inactive",
    val create_dttm: LocalDateTime = LocalDateTime.now(),
    val assign_dttm: LocalDateTime? = null,
    val planned_start_dt: LocalDate? = null,
    val planned_close_dt: LocalDate? = null,
    val close_dttm: LocalDateTime? = null,
    val task_above: Task? = null,
    val task_below: Task? = null,
    val progress: Int = 0

) {

    val isClosed: Boolean
        get() = (close_dttm != null)

    val isComplete: Boolean
        get() = (progress == 100)


    fun setCloseDttm(close_dttm: LocalDateTime) : Task{
        if(close_dttm >= this.create_dttm){
            return this.copy(close_dttm = close_dttm)
        }
        else {
            return this
        }
    }
    fun assign(assign_dttm: LocalDateTime) : Task{
        if(assign_dttm >= this.create_dttm && !isClosed){
            return this.copy(assign_dttm = assign_dttm)
        }
        else{
            return this
        }
    }
    fun setPlannedStartDt(planned_start_dt : LocalDate) : Task{
        if(planned_start_dt <= this.planned_close_dt){
            return this.copy(planned_start_dt = planned_start_dt)
        }
        else{
            return this
        }
    }
    fun setPlannedCloseDt(planned_close_dt : LocalDate) : Task{
        if(planned_close_dt >= this.planned_start_dt){
            return this.copy(planned_close_dt = planned_close_dt)
        }
        else {
            return this
        }
    }
    fun setProgress(progress : Int) : Task{
        if(progress <= 100 && progress >= 0)
        {
            return this.copy(progress = progress)
        }
        else{
            return this
        }
    }
    fun setApprasial(apprasial : Double) : Task{
        if(apprasial > 0)
        {
            return this.copy(apprasial = apprasial)
        }
        else{
            return this
        }
    }

    fun getFullInfo(): String {
        val prop_list = listOf(
            taskNumber,
            name,
            description,
            creator.userId.toString(),
            priority.toString(),
            executor?.userId.toString(),
            apprasial.toString(),
            status,
            create_dttm.toString(),
            assign_dttm.toString(),
            planned_start_dt.toString(),
            planned_close_dt.toString(),
            close_dttm.toString(),
            task_above?.taskNumber.toString(),
            task_below?.taskNumber.toString(),
            progress.toString()
        )
        return prop_list.joinToString(";")
    }

}