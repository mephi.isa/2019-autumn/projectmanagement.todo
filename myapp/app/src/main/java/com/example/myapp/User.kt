package com.example.myapp

data class User(
    val userId: Int = 0,
    val lastName: String,
    val firstName: String,
    val middleName: String,
    val role: String,
    val manager: User?
) {
    fun getFullName() = listOf(lastName, firstName, middleName).joinToString(" ")
}