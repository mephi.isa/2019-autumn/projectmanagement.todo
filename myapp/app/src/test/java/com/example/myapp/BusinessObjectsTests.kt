package com.example.myapp

import com.example.myapp.generator.UserGenerator
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime


@Suppress("MemberVisibilityCanBePrivate")
class BusinessObjectsTests {
    val tasklist: ArrayList<Task> = arrayListOf()

    val createDttm = LocalDateTime.of(
        2019,
        9,
        1,
        12,
        0,
        2
    )

    val assignDttm = LocalDateTime.of(2019, 9, 3, 11, 15, 23)
    val planned_start_dt = LocalDate.of(2019, 10, 1)
    val planned_close_dt = LocalDate.of(2019, 11, 1)
    val userManager = User(
        userId = 18232,
        lastName = "ManLN",
        firstName = "ManFN",
        middleName = "ManMN",
        role = "Manager",
        manager = null
    )
    val userEmp = UserGenerator.createUser(
        userId = 27346,
        lastName = "EmpLN",
        firstName = "EmpFN",
        middleName = "EmpMN",
        role = "Employee",
        manager = userManager
    )
    val userEmp2 = UserGenerator.createUser(
        lastName = "LNM",
        firstName = "FNM",
        middleName = "MNM",
        role = "Employee",
        manager = null
    )
    val task1 = Task(
        1765,
        "TASK1",
        "DESC1",
        userManager,
        6,
        userEmp,
        0.0,
        "inactive",
        LocalDateTime.of(2019, 10, 1,0,0,0),
        LocalDateTime.of(2019, 10, 3,0,0,0),
        LocalDate.of(2019, 10, 10),
        LocalDate.of(2019, 11, 1),
        null,
        null,
        null,
        0
    )
    val task_s = Task(
        taskNumber = 999,
        name = "short_task",
        creator = userEmp
    )
    val task2 = Task(
        1766, "TASK2", "DESC2", userManager, 4, userEmp, 0.0,
        "active", createDttm, assignDttm, planned_start_dt, planned_close_dt, null,
        null, null, 95
    )
    val task3 = Task(
        1767,
        "TASK3",
        "DESC3",
        userManager,
        2,
        userEmp,
        0.0,
        "active",
        createDttm.plusDays(20),
        assignDttm,
        planned_start_dt.minusDays(5),
        planned_close_dt.plusDays(5),
        null,
        null,
        null,
        0
    )

    @Before
    fun prepareForTest() {
      // TODO Перенести инициализацию сюда.
    }
    @Test
    fun shortConstructorUser_isCorrect(){
        assertEquals("LNM",userEmp2.lastName)
        assertEquals("FNM",userEmp2.firstName)
        assertEquals("MNM",userEmp2.middleName)
        assertEquals("Employee",userEmp2.role)
        assertEquals(null,userEmp2.manager)
    }

    @Test
    fun fullConstructorUser_isCorrect(){
        assertEquals(27346,userEmp.userId)
        assertEquals("EmpLN",userEmp.lastName)
        assertEquals("EmpFN",userEmp.firstName)
        assertEquals("EmpMN",userEmp.middleName)
        assertEquals("Employee",userEmp.role)
        assertEquals(userManager,userEmp.manager)
    }
    @Test
    fun shortConstructorTask_isCorrect(){
        assertEquals(999, task_s.taskNumber)
        assertEquals("short_task", task_s.name)
        assertEquals(userEmp, task_s.creator)
    }
    @Test
    fun fullname_isCorrect() = assertEquals("EmpLN EmpFN EmpMN", userEmp.getFullName())

    @Test
    fun manager_isCorrect() {
        val userEmp_n = userEmp.copy(manager = userManager)
        assertEquals(18232, userEmp_n.manager?.userId)
    }

    @Test
    fun role_isCorrect() {
        val userEmp_n = userEmp.copy(role = "new_role")
        assertEquals("new_role", userEmp_n.role)
    }

    // FullInfo + конструктор, как следствие
    @Test
    fun fullInfo_isCorrect() {
        val fullInfo = task2.getFullInfo()
        assertEquals(
            "1766;TASK2;DESC2;18232;4;27346;0.0;active;2019-09-01T12:00:02;2019-09-03T11:15:23;2019-10-01;2019-11-01;null;null;null;95",
            fullInfo
        )
    }

    @Test
    fun close_correct() = assertEquals(false, task1.isClosed)

    @Test
    fun complete_correct() = assertEquals(false, task2.isComplete)

    @Test
    fun tasknum_isCorrect() = assertEquals(1765, task1.taskNumber)

    @Test
    fun setname_isCorrect() {
        val task_new = task1.copy(name = "new name")
        assertEquals("new name", task_new.name)
    }

    @Test
    fun setdesc_isCorrect() {
        val task_new = task1.copy(description = "NEW DESCRIPTION")
        assertEquals("NEW DESCRIPTION", task_new.description)
    }

    @Test
    fun setpriority_isCorrect() {
        val task_new = task1.copy(priority = 7)
        assertEquals(7, task_new.priority)
    }

    @Test
    fun setexecutor_isCorrect() {
        val task_new = task1.copy(executor = userManager)
        assertEquals(18232, task_new.executor?.userId)
    }

    @Test
    fun setapprasial_isCorrect() {
        val task_new = task1.setApprasial(44.6)
        assertEquals(44.6, task_new.apprasial, 0.01)
    }

    @Test
    fun setstatus_isCorrect() {
        val task_new = task1.copy(status = "new_status")
        assertEquals("new_status", task_new.status)
    }

    @Test
    fun set_create_dttm_isCorrect() {
        val task_new = task1.copy(create_dttm = LocalDateTime.of(2019, 9, 21, 12, 15, 18))
        assertEquals("2019-09-21T12:15:18", task_new.create_dttm.toString())
    }

    @Test
    fun set_assign_dttm_isCorrect() {
        val task_new = task1.assign(LocalDateTime.of(2019, 10, 5, 12, 15, 18))
        assertEquals("2019-10-05T12:15:18", task_new.assign_dttm.toString())
    }
    @Test
    fun set_assign_dttm_isNotCorrect() {
        val task_new = task1.assign(LocalDateTime.of(2014, 10, 5, 12, 15, 18))
        assertNotEquals("2014-10-05T12:15:18", task_new.assign_dttm.toString())
    }

    @Test
    fun set_close_dttm_isCorrect() {
        val task_new = task1.setCloseDttm(LocalDateTime.of(2019, 10, 21, 12, 15, 18))
        assertEquals("2019-10-21T12:15:18", task_new.close_dttm.toString())
        assertTrue(task_new.isClosed)
    }

    @Test
    fun set_close_dttm_isNotCorrect() {
        val task_new = task1.setCloseDttm(LocalDateTime.of(2015, 10, 21, 12, 15, 18))
        assertNotEquals("2015-10-21T12:15:18", task_new.close_dttm.toString())
    }

    @Test
    fun set_plan_start_dt_isCorrect() {
        val task_new = task1.setPlannedStartDt(LocalDate.of(2019, 10, 11))
        assertEquals("2019-10-11", task_new.planned_start_dt.toString())
    }
    @Test
    fun set_plan_start_dt_isNotCorrect() {
        val task_new = task1.setPlannedStartDt(LocalDate.of(2029, 10, 11))
        assertNotEquals("2029-10-11", task_new.planned_start_dt.toString())
    }

    @Test
    fun set_plan_close_dt_isCorrect() {
        val task_new = task1.setPlannedCloseDt(LocalDate.of(2019, 11, 26))
        assertEquals("2019-11-26", task_new.planned_close_dt.toString())
    }

    @Test
    fun set_plan_close_dt_isNotCorrect() {
        val task_new = task1.setPlannedCloseDt(LocalDate.of(2015, 11, 26))
        assertNotEquals("2015-11-26", task_new.planned_close_dt.toString())
    }

    @Test
    fun set_task_above_isCorrect() {
        val task_new = task1.copy(task_above = task2)
        assertEquals(1766, task_new.task_above?.taskNumber)
    }

    @Test
    fun set_task_below_isCorrect() {
        val task_new = task1.copy(task_below = task2)
        assertEquals(1766, task_new.task_below?.taskNumber)
    }

    @Test
    fun setProgress_isCorrect() {
        val task_new = task1.setProgress(84)
        assertEquals(84, task_new.progress)
    }
}
