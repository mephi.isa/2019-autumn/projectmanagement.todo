package com.example.myapp.generator

import com.example.myapp.User
import com.example.myapp.Task
import java.time.LocalDate
import java.time.LocalDateTime

object UserGenerator {

    fun createUser(userId:Int = 0,
                   lastName:String = "",
                   firstName:String = "",
                   middleName:String = "",
                   role:String = "",
                   manager:User? = null): User {

        return User(
            userId,
            lastName,
            firstName,
            middleName,
            role,
            manager
        )
    }

}

object TaskGenerator {

    fun createTask(taskNumber: Int = 0,
                   name: String = "",
                   description: String = "",
                   creator: User = UserGenerator.createUser(),
                   priority: Int = 0,
                   executor: User? = null,
                   apprasial: Double = 0.0,
                   status: String = "inactive",
                   create_dttm: LocalDateTime = LocalDateTime.now(),
                   assign_dttm: LocalDateTime? = null,
                   plannedStartDt: LocalDate? = null,
                   planned_close_dt: LocalDate? = null,
                   close_dttm: LocalDateTime? = null,
                   task_above: Task? = null,
                   task_below: Task? = null,
                   progress: Int = 0): Task {

        return Task(
            taskNumber,
            name,
            description,
            creator,
            priority,
            executor,
            apprasial,
            status,
            create_dttm,
            assign_dttm,
            plannedStartDt,
            planned_close_dt,
            close_dttm,
            task_above,
            task_below,
            progress
        )

    }

}