﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListNamespace;

namespace ToDoListWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserTasksController : ControllerBase
    {
        // GET: api/UserTasks
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/UserTasks/5
        [HttpGet("{id}", Name = "GetUserTasks")]
        public IEnumerable<TaskInfo> Get(int id)
        {
            API Api = new API();
            Api.ConnectDatabase(Startup.TestDatabase);
            return Api.GetUserTasksInfo(id);
        }

        // POST: api/UserTasks
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/UserTasks/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
