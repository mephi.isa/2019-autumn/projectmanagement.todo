﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListNamespace;

namespace ToDoListWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrController : ControllerBase
    {
        // GET: api/Registr
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Registr/5
        [HttpGet("{id}", Name = "GetRegistr")]
        public string Get(int id)
        {
            return "value";
        }

        public class UserRegistr
        {
            public string First_name { get; set; }
            public string Middle_name { get; set; }
            public string Last_name { get; set; }
            public string Role { get; set; }
            public int ManagerId { get; set; }
            public string login { get; set; }
            public string password { get; set; }
        }

        // POST: api/Registr
        [HttpPost]
        public UserInfo Post(UserRegistr UserReg)
        {
            API Api = new API();
            Api.ConnectRegistrationDatabase();
            Api.ConnectDatabase(Startup.TestDatabase);

            int id = Api.Registration(UserReg.login, UserReg.password, UserReg.Last_name, UserReg.First_name, UserReg.Middle_name, UserReg.Role, UserReg.ManagerId);
            if (id == -1)
                return null;
            else
                return Api.GetUserInfoById(id);
        }

        // PUT: api/Registr/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
