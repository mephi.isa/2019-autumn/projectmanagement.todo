﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListNamespace;

namespace ToDoListWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SwapController : ControllerBase
    {
        // GET: api/Swap
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Swap/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        public class SwapMes
        {
            public int num1 { get; set; }
            public int num2 { get; set; }
        }

        // POST: api/Swap
        [HttpPost]
        public void Post(SwapMes mes)
        {
            API Api = new API();
            Api.ConnectDatabase();

            Api.SwapTasks(mes.num1, mes.num2);
        }

        // PUT: api/Swap/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
