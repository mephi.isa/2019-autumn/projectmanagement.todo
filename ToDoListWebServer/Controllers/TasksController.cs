﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListNamespace;

namespace ToDoListWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        // GET: api/Tasks
        [HttpGet]
        public IEnumerable<TaskInfo> Get()
        {
            API Api = new API();
            Api.ConnectDatabase(Startup.TestDatabase);
            return Api.GetTasksInfo().ToArray();
        }

        // GET: api/Tasks/5
        [HttpGet("{id}", Name = "GetTasksInfo")]
        public TaskInfo Get(int id)
        {
            API Api = new API();
            Api.ConnectDatabase(Startup.TestDatabase);
            return Api.GetTaskInfoByNum(id);
        }

        public class TaskMessage
        {
            public int WhoId { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public int Priority { get; set; }
            public int? ExecutorId { get; set; }
            public double Apprasial { get; set; }
            public int Progress { get; set; }
            public string Status { get; set; }
            public string Assign_dttm { get; set; }
            public string Planned_start { get; set; }
            public string Planned_close { get; set; }
            public int? TaskAboveNum { get; set; }
            public int? TaskBelowNum { get; set; }
            public bool Close { get; set; }

            public TaskMessage()
            {
                Description = "";
                Priority = -1;
                Apprasial = -1;
                Progress = 0;
                Status = "";
                Assign_dttm = "";
                Planned_start = "";
                Planned_close = "";
            }
                
        }

        // POST: api/Tasks
        [HttpPost]
        public int Post(TaskMessage task)
        {
            API Api = new API();
            Api.ConnectDatabase(Startup.TestDatabase);

            return Api.CreateTask(task.Name, task.WhoId, task.Description, task.Priority, task.ExecutorId, task.Apprasial, task.Progress, task.Status,
                task.Assign_dttm, task.Planned_start, task.Planned_close);
        }

        // PUT: api/Tasks/5
        [HttpPut("{id}")]
        public int Put(int id, TaskMessage task)
        {
            API Api = new API();
            Api.ConnectDatabase(Startup.TestDatabase);

            int res = Api.EditTask(task.WhoId, id, task.Name, task.Description, task.Priority, task.ExecutorId, task.Apprasial, task.Progress, task.Status,
                task.Assign_dttm, task.Planned_start, task.Planned_close);

            if (res != 0)
                return res;

            if (task.Close)
                res = Api.CloseTask(id);

            return res;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
