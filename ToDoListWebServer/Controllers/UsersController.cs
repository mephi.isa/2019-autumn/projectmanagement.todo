﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListNamespace;

namespace ToDoListWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        // GET: api/Users
        [HttpGet]
        public IEnumerable<UserInfo> Get()
        {
            API Api = new API();
            Api.ConnectDatabase(Startup.TestDatabase);
            return Api.GetUsersInfo().ToArray();
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUsersInfo")]
        public UserInfo Get(int id)
        {
            API Api = new API();
            Api.ConnectDatabase(Startup.TestDatabase);
            return Api.GetUserInfoById(id);
        }

        // POST: api/Users
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
