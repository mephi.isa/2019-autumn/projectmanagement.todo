﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListNamespace;

namespace ToDoListWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubuserTasksController : ControllerBase
    {
        // GET: api/SubuserTasks
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SubuserTasks/5
        [HttpGet("{id}", Name = "GetSubuserTasks")]
        public IEnumerable<TaskInfo> Get(int id)
        {
            API Api = new API();
            
            Api.ConnectDatabase(Startup.TestDatabase);

            Dictionary<int, List<TaskInfo>> dict = Api.GetSubuserTasksInfo(id);
            if (dict == null)
                return null;

            List<TaskInfo> tasksInfo = new List<TaskInfo>();
            foreach (var list in dict.Values)
                tasksInfo.AddRange(list);
            return tasksInfo;
        }

        // POST: api/SubuserTasks
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/SubuserTasks/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
