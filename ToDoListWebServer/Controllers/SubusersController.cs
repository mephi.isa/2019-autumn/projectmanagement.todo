﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListNamespace;

namespace ToDoListWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubusersController : ControllerBase
    {
        // GET: api/Subusers
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Subusers/5
        [HttpGet("{id}", Name = "GetSubusers")]
        public IEnumerable<UserInfo> Get(int id)
        {
            API Api = new API();
            Api.ConnectDatabase(Startup.TestDatabase);
            return Api.GetSubusersInfo(id);
        }

        // POST: api/Subusers
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Subusers/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
