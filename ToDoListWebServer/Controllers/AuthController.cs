﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListNamespace;

namespace ToDoListWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        // GET: api/Auth
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Auth/5
        [HttpGet("{id}", Name = "GetAuth")]
        public string Get(int id)
        {
            return "value";
        }

        public class LogPass
        {
            public string login { get; set; }
            public string password { get; set; }
        };

        // POST: api/Auth
        [HttpPost]
        public UserInfo Post(LogPass logpass)
        {
            API Api = new API();
            Api.ConnectRegistrationDatabase();
            Api.ConnectDatabase();

            int id = Api.Authorizate(logpass.login, logpass.password);
            if (id == -1)
                return null;
            else
                return Api.GetUserInfoById(id);
        }

        // PUT: api/Auth/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
