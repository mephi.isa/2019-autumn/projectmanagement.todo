﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ToDoListNamespace;

namespace ToDoWebApp
{
    public partial class EditingTask : System.Web.UI.Page
    {
        string Id;
        string Number;

        static int PredExec;
        static string PredAssign;
        protected void Page_Load(object sender, EventArgs e)
        {
            Id = Request.QueryString["userId"];
            Number = Request.QueryString["Number"];
            Id = Request.QueryString["userId"];

            string json = GetTaskInfoStr();
            TaskInfo task = new JavaScriptSerializer().Deserialize<TaskInfo>(json);

            bSave.Click += bSaveClick;
            bCancel.Click += bCancelClick;

            if (!Page.IsPostBack)
            {
                List<string> Fios = new List<string> { "" };
                List<UserInfo> subusers = GetSubusers(task.CreatorId);

                string jsonuser = GetUserInfoStr(task.CreatorId);
                UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(jsonuser);

                Fios.Add(user.FIO);
                foreach (var subuser in subusers)
                    Fios.Add(subuser.FIO);

                inpExec.DataSource = Fios;
                inpExec.DataBind();

                List<string> statuses = new List<string> { "Активно", "Неактивно" };
                inpStatus.DataSource = statuses;
                inpStatus.DataBind();

                UserInfo creator = new JavaScriptSerializer().Deserialize<UserInfo>(GetUserInfoStr(task.CreatorId));
                UserInfo executor = new JavaScriptSerializer().Deserialize<UserInfo>(GetUserInfoStr(task.ExecutorId));

                if (int.Parse(Id) != creator.Id)
                    inpExec.Enabled = false;

                PredExec = executor.Id;
                PredAssign = task.Assign_dttm == null ? "" : task.Assign_dttm;

                inpName.Text = task.Name;
                inpDesc.Text = task.Description;
                inpPrior.Text = task.Priority.ToString();
                inpExec.Text = executor.FIO;
                inpAppr.Text = task.Apprasial.ToString();
                inpProgress.Text = task.Progress.ToString();
                inpStatus.Text = task.Status == "acive" ? "Активно" : "Неактивно";
                inpStartDttm.Text = task.Planned_start_dt;
                inpCloseDttm.Text = task.Planned_close_dt;
                inpClose.Checked = false;
            }
        }

        void bCancelClick(Object sender, EventArgs e)
        {
            Response.Redirect("TasksList.aspx?userId=" + Id.ToString());
        }
        void bSaveClick(Object sender, EventArgs e)
        {
            string res = SendEditRequest();
            if (res.Contains("Ошибка"))
                lError.Text = res;
            else if (res == "0")
                lError.Text = "Изменения сохранены";
            else
                lError.Text = "Ошибка! Код " + res;
        }

        private string GetUserInfoStr(int Id)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/users/" + Id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }

        private string GetTaskInfoStr()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/tasks/" + Number);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }

        private string SendEditRequest()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/Tasks/" + Number);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "PUT";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string status = "active";
                switch (inpStatus.Text)
                {
                    case "Активно":
                        status = "active";
                        break;
                    case "Неактивно":
                        status = "inactive";
                        break;
                    default:
                        status = "";
                        break;
                }

                int prior = -1, appr = -1, progress = 0;
                if (inpPrior.Text != "")
                    if (!int.TryParse(inpPrior.Text, out prior))
                        return "Ошибка! Неверно заполнено поле \"Приоритет\"";

                if (inpAppr.Text != "")
                    if (!int.TryParse(inpAppr.Text, out appr))
                        return "Ошибка! Неверно заполнено поле \"Оценка\"";

                if (inpProgress.Text != "")
                    if (!int.TryParse(inpProgress.Text, out progress))
                        return "Ошибка! Неверно заполнено поле \"Прогресс\"";

                if (progress < 0 || progress > 100)
                    return "Ошибка! Значение \"Прогресс\" должно находится в интервале от 0 до 100";

                int ExecId = -1;
                if (inpExec.Text == "")
                    return "Ошибка! Незаполнено поле \"Исполнитель\"";
                else
                {
                    string jsontask = GetTaskInfoStr();
                    TaskInfo task = new JavaScriptSerializer().Deserialize<TaskInfo>(jsontask);

                    List<UserInfo> execs = GetSubusers(task.CreatorId);
                    string jsonUserStr = GetUserInfoStr(task.CreatorId);
                    UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(jsonUserStr);

                    execs.Add(user);
                    UserInfo FindUser = execs.FirstOrDefault(a => a.FIO == inpExec.Text);
                    if (FindUser == null)
                        return "Ошибка! Исполнитель не найден";
                    ExecId = FindUser.Id;
                }

                DateTime test;
                string json = new JavaScriptSerializer().Serialize(new
                {
                    WhoId = int.Parse(Id),
                    Name = inpName.Text,
                    Description = inpDesc.Text,
                    Priority = prior,
                    ExecutorId = ExecId,
                    Apprasial = appr,
                    Progress = progress,
                    Status = status,
                    Assign_dttm = PredExec == ExecId ? PredAssign : DateTime.Now.ToString(),
                    Planned_start = DateTime.TryParse(inpStartDttm.Text, out test) ? inpStartDttm.Text : "",
                    Planned_close = DateTime.TryParse(inpCloseDttm.Text, out test) ? inpCloseDttm.Text : "",
                });
                streamWriter.Write(json);
            }

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
            return result;
        }

        private List<UserInfo> GetSubusers(int usId)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/subusers/" + usId);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
            List<UserInfo> users = new JavaScriptSerializer().Deserialize<List<UserInfo>>(result);

            return users;
        }
    }
}