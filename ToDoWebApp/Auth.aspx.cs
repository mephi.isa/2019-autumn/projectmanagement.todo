﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using ToDoListNamespace;

namespace ToDoWebApp
{
    public partial class Auth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bEnter.Click += bEnter_Click;
            bRegistr.Click += bRegistr_Click;
        }

        void bEnter_Click(Object sender, EventArgs e)
        {
            string JsonRes = CheckAuth();

            UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(JsonRes);
            if (user == null)
                lError.Text = "Пара логин/пароль не найдена.";
            else
            {
                Response.Redirect("TasksList.aspx?userId=" + user.Id.ToString());
            }
        }

        void bRegistr_Click(Object sender, EventArgs e)
        {
            Response.Redirect("Registration.aspx");
        }

        private string CheckAuth()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/auth");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //string json = "{\"login\": \"petrov\", \"password\": \"pet123\"}";
                string json = new JavaScriptSerializer().Serialize(new
                {
                    Login = inpLogin.Text,
                    Password = inpPassword.Text,
                });
                streamWriter.Write(json);
            }

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
            return result;
        }
    }
}