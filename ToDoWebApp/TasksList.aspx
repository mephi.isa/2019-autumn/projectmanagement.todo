﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TasksList.aspx.cs" Inherits="ToDoWebApp.TasksList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles.css" />
</head>
<body>
    <form id="rsvpform" runat="server">
        <div>
            <asp:Label runat="server" id="lHello"/>
            <asp:Button ID="bSubusersTasks" runat="server" Text="Задачи подчиненных" Visible="false"/>
            <p>Ваш список задач:</p>
        </div>
        <div>
            <%
                foreach (SimpleTask task in GetTasks())
                {
                    Response.Write(String.Format(@"
                        <div class='item'>
                            <h3>{0}</h3>
                            {1}
                        <p><a href='{5}AboutTask.aspx?Number={2}&userId={3}&PredPage=MyTasks'>Подробнее</a></p>
                        <p><a href='{5}TasksList.aspx?userId={3}&Num={4}&Swap=swap&Dir=Up'>Вверх</a></p>
                        <p><a href='{5}TasksList.aspx?userId={3}&Num={4}&Swap=swap&Dir=Down'>Вниз</a></p>
                        <p>============================================================</p>
                        </div>", 
                        task.Name, task.Desc, task.Number, task.Executor, task.Above, PathMyself));
                }
            %>
        </div>
        <div>
            <asp:Button ID="bAddTask" runat="server" Text="Добавить"/>
            <br />
            <br />
            <br />
            <asp:Button ID="bExit" runat="server" Text="Выйти"/>
        </div>
    </form>
</body>
</html>
