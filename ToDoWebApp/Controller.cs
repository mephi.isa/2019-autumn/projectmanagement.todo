﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoWebApp
{
    public class Controller
    {
        static bool Debug = false;
        public static string Path = Debug ? "http://localhost:52811/" : "http://localhost:8000/";
        public static string PathMyself = Debug ? "https://localhost:44393/" : "https://089aa185.ngrok.io/";
    }
}