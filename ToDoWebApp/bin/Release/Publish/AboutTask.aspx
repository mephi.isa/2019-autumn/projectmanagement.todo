﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AboutTask.aspx.cs" Inherits="ToDoWebApp.AboutTask" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles.css" />
</head>
<body>
    <form id="rsvpform" runat="server">
        <div>
            <h1>Детали задачи:</h1>
        </div>
        <div class="reg_table">
            <div class="field">
                <label>Создатель:</label>
                <asp:Label id="inpCreator" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Название:</label>
                <asp:Label id="inpName" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Дата создания:</label>
                <asp:Label id="inpCreateDate" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Описание:</label>
                <asp:Label id="inpDesc" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Приоритет:</label>
                <asp:Label id="inpPrior" runat="server" CssClass="inp_element" TextMode="Number"/>
            </div>
            <br />
            <div class="field">
                <label>Оценка:</label>
                <asp:Label id="inpAppr" runat="server" CssClass="inp_element" TextMode="Number"/>
            </div>
            <br />
            <div class="field">
                <label>Исполнитель:</label>
                <asp:Label id="inpExec" runat="server" CssClass="inp_element" />
            </div>
            <br />
            <div class="field">
                <label>Статус:</label>
                <asp:Label id="inpStatus" runat="server" CssClass="inp_element" />
            </div>
            <br />
            <div class="field">
                <label>Прогресс:</label>
                <asp:Label id="inpProgress" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Дата назначения:</label>
                <asp:Label id="inpAssign" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Планируемая дата начала:</label>
                <asp:Label id="inpStartDttm" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Планируемая дата завершения:</label>
                <asp:Label id="inpCloseDttm" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Дата завершения:</label>
                <asp:Label id="inpClose" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <asp:Label id="lError" runat="server" Text=" " ForeColor="Red"></asp:Label>
            <br />
            <div>
                <asp:Button ID="bBack" runat="server" Text="Назад"/>
                <asp:Button ID="bEdit" runat="server" Text="Редактировать"/>
            </div>
        </div>
    </form>
</body>
</html>
