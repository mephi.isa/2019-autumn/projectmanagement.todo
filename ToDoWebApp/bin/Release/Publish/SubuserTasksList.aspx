﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubuserTasksList.aspx.cs" Inherits="ToDoWebApp.SubuserTasksList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles.css" />
</head>
<body>
    <form id="rsvpform" runat="server">
        <div>
            <asp:Button ID="bMyTasks" runat="server" Text="Ваш список задач"/>
            <p>Список задач ваших подчиненных:</p>
        </div>
        <div>
            <%
                foreach (SimpleTask task in GetTasks())
                {
                    Response.Write(String.Format(@"
                        <div class='item'>
                            <h3>{0}</h3>
                            <p>Исполнитель: {1}</p>
                        <p><a href='{4}AboutTask.aspx?Number={2}&userId={3}&PredPage=SubusersTasks'>Подробнее</a></p>
                        <p>============================================================</p>
                        </div>", 
                        task.Name, task.Executor, task.Number, task.WhoId, PathMyself));
                }
            %>
        </div>

        <div>
            <asp:Button ID="bAddTask" runat="server" Text="Добавить"/>
            <br />
            <br />
            <br />
            <asp:Button ID="bExit" runat="server" Text="Выйти"/>
        </div>
    </form>
</body>
</html>
