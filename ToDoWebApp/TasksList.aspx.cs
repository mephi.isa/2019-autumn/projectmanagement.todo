﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ToDoListNamespace;

namespace ToDoWebApp
{
    public partial class TasksList : System.Web.UI.Page
    {
        string Id;
        public string PathMyself;
        protected void Page_Load(object sender, EventArgs e)
        {
            PathMyself = Controller.PathMyself;
            Id = Request.QueryString["userId"];
            string swap = Request.QueryString["Swap"];
            if (swap != null)
            {
                string Num = Request.QueryString["Num"];
                string Dir = Request.QueryString["Dir"];

                SendSwapRequest(int.Parse(Num), Dir);
                Response.Redirect("TasksList.aspx?userId=" + Id.ToString());
                return;
            }

            string json = GetUserInfoStr();
            UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(json);
            if (user.Role == "Manager")
            {
                bSubusersTasks.Visible = true;
                bSubusersTasks.Enabled = true;
                bAddTask.Visible = true;
                bAddTask.Enabled = true;
            }
            else
            {
                bSubusersTasks.Visible = false;
                bSubusersTasks.Enabled = false;
                bAddTask.Visible = false;
                bAddTask.Enabled = false;
            }
            lHello.Text = "<h1>Здравствуйте, " + user.FIO + "</h1>";
            bSubusersTasks.Click += bSubusersTasksClick;
            bExit.Click += bExitClick;
            bAddTask.Click += bAddTaskClick;
        }

        private string GetUserInfoStr()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/users/" + Id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }

        public class SimpleTask
        {
            public string Name;
            public string Desc;
            public int Number;
            public int Executor;
            public int Above;

            public SimpleTask(string name, string desc, int num, int exec, int above)
            {
                Name = name;
                Desc = desc;
                Number = num;
                Executor = exec;
                Above = above;
            }
        }

        public List<SimpleTask> GetTasks()
        {
            string json = GetTasksJson();

            List<TaskInfo> tasks = new JavaScriptSerializer().Deserialize<List<TaskInfo>>(json);
            List<SimpleTask> simpleTasks = new List<SimpleTask>();

            foreach (TaskInfo task in tasks)
            {
                simpleTasks.Add(new SimpleTask(task.Name, task.Description, task.TaskNumber, task.ExecutorId, task.Above));
            }

            return simpleTasks.OrderBy(a => a.Above).ToList();
        }

        private string GetTasksJson()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/usertasks/" + Id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }

        void bSubusersTasksClick(Object sender, EventArgs e)
        {
            Response.Redirect("SubuserTasksList.aspx?userId=" + Id.ToString());
        }

        void bExitClick(Object sender, EventArgs e)
        {
            Response.Redirect("Auth.aspx");
        }

        void bAddTaskClick(Object sender, EventArgs e)
        {
            Response.Redirect("AddingTask.aspx?userId=" + Id.ToString());
        }

        private void SendSwapRequest(int Num1, string Dir)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/swap");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            int Num2;
            List<SimpleTask> Tasks = GetTasks();
            int ind = -1;
            if (Dir == "Up")
            {
                ind = Tasks.IndexOf(Tasks.FirstOrDefault(a => a.Above == Num1)) - 1;
                if (ind < 0)
                    return;
            }
            else if (Dir == "Down")
            {
                ind = Tasks.IndexOf(Tasks.FirstOrDefault(a => a.Above == Num1)) + 1;
                if (ind >= Tasks.Count)
                    return;
            }
            Num2 = Tasks[ind].Above;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    num1 = Num1,
                    num2 = Num2
                });
                streamWriter.Write(json);
            }

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
        }
    }
}