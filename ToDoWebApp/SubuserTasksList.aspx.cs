﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ToDoListNamespace;

namespace ToDoWebApp
{
    public partial class SubuserTasksList : System.Web.UI.Page
    {
        string Id;
        public string PathMyself;
        protected void Page_Load(object sender, EventArgs e)
        {
            PathMyself = Controller.PathMyself;
            Id = Request.QueryString["userId"];

            string json = GetUserInfoStr();
            UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(json);
            bMyTasks.Click += bMyTasksClick;
            bExit.Click += bExitClick;
            bAddTask.Click += bAddTaskClick;
        }

        private string GetUserInfoStr()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/users/" + Id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }

        public class SimpleTask
        {
            public string Name;
            public string Desc;
            public int Number;
            public string Executor;
            public int WhoId;

            public SimpleTask(string name, string desc, int num, string exec, int who)
            {
                Name = name;
                Desc = desc;
                Number = num;
                Executor = exec;
                WhoId = who;
            }
        }

        public List<SimpleTask> GetTasks()
        {
            string json = GetTasksJson();

            List<TaskInfo> tasks = new JavaScriptSerializer().Deserialize<List<TaskInfo>>(json);
            List<SimpleTask> simpleTasks = new List<SimpleTask>();

            foreach (TaskInfo task in tasks)
            {
                simpleTasks.Add(new SimpleTask(task.Name, task.Description, task.TaskNumber, GetUserById(task.ExecutorId), int.Parse(Id)));
            }

            return simpleTasks;
        }

        private string GetUserById(int id)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/users/" + id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
            UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(result);
            return user.FIO;
        }

        private string GetTasksJson()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/subusertasks/" + Id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }

        void bMyTasksClick(Object sender, EventArgs e)
        {
            Response.Redirect("TasksList.aspx?userId=" + Id.ToString());
        }
        
        void bExitClick(Object sender, EventArgs e)
        {
            Response.Redirect("Auth.aspx");
        }

        void bAddTaskClick(Object sender, EventArgs e)
        {
            Response.Redirect("AddingTask.aspx?userId=" + Id.ToString());
        }
    }
}