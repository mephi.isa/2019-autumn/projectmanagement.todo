﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ToDoListNamespace;

namespace ToDoWebApp
{
    public partial class Registration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bRegistr.Click += bRegistr_Click;
            bEnter.Click += bEnter_Click;

            if (!Page.IsPostBack)
            {
                List<string> Fios = new List<string> { "" };
                List<UserInfo> managers = GetManagers();

                foreach (var manager in managers)
                    Fios.Add(manager.FIO);

                inpBoss.DataSource = Fios;
                inpBoss.DataBind();

                List<string> roles = new List<string> { "Работник", "Начальник" };
                inpRole.DataSource = roles;
                inpRole.DataBind();
            }
        }

        void bRegistr_Click(Object sender, EventArgs e)
        {
            if (inpPassword.Text != inpConfirmPassword.Text)
            {
                lError.Text = "Пароли не совпадают";
                return;
            }

            string JsonRes = SendRegistr();

            UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(JsonRes);
            if (user == null)
                lError.Text = "Ошибка регистрации.";
            else
            {
                Response.Redirect("TasksList.aspx?userId=" + user.Id.ToString());
            }
        }

        private string SendRegistr()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/registr");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string role = "Employee";
                switch (inpRole.Text)
                {
                    case "Работник":
                        role = "Employee";
                        break;
                    case "Начальник":
                        role = "Manager";
                        break;
                    default:
                        role = "";
                        break;
                }

                int BossId = -1;
                if (inpBoss.Text != "")
                    BossId = GetManagers().FirstOrDefault(a => a.FIO == inpBoss.Text).Id;
                User user = new User(inpLastname.Text, inpFirstname.Text, inpMiddlename.Text, role, null);
                string json = new JavaScriptSerializer().Serialize(new
                {
                    login = inpLogin.Text,
                    password = inpPassword.Text,
                    First_name = user.First_name,
                    Middle_name = user.Middle_name,
                    Last_name = user.Last_name,
                    Role = user.Role,
                    ManagerId = BossId
                });
                streamWriter.Write(json);
            }

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
            return result;
        }

        private List<UserInfo> GetManagers()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/users");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
            List<UserInfo> users = new JavaScriptSerializer().Deserialize<List<UserInfo>>(result);

            return users.Where(a => a.Role == "Manager").ToList();
        }

        void bEnter_Click(Object sender, EventArgs e)
        {
            Response.Redirect("Auth.aspx");
        }
    }
}