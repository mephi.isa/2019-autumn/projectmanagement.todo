﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ToDoListNamespace;

namespace ToDoWebApp
{
    public partial class AboutTask : System.Web.UI.Page
    {
        string Number;
        string Id;
        string PredPage;
        protected void Page_Load(object sender, EventArgs e)
        {
            Number = Request.QueryString["Number"];
            Id = Request.QueryString["userId"];
            PredPage = Request.QueryString["PredPage"];

            string json = GetTaskInfoStr();
            TaskInfo task = new JavaScriptSerializer().Deserialize<TaskInfo>(json);

            bEdit.Click += bEditClick;
            bBack.Click += bBackClick;

            UserInfo creator = new JavaScriptSerializer().Deserialize<UserInfo>(GetUserInfoStr(task.CreatorId));
            UserInfo executor = new JavaScriptSerializer().Deserialize<UserInfo>(GetUserInfoStr(task.ExecutorId));

            inpCreator.Text = creator.FIO;
            inpName.Text = task.Name;
            inpDesc.Text = task.Description;
            inpPrior.Text = task.Priority.ToString();
            inpExec.Text = executor.FIO;
            inpAppr.Text = task.Apprasial.ToString();
            inpProgress.Text = task.Progress.ToString();
            switch (task.Status)
            {
                case "active": 
                    inpStatus.Text = "Активна";
                    break;
                case "inactive":
                    inpStatus.Text = "Неактивна";
                    break;
            }
            inpAssign.Text = task.Assign_dttm;
            inpStartDttm.Text = task.Planned_start_dt == System.Data.SqlTypes.SqlDateTime.MinValue.Value.ToString() ? "Не назначена" : task.Planned_start_dt;
            inpCreateDate.Text = task.Create_dttm == System.Data.SqlTypes.SqlDateTime.MinValue.Value.ToString() ? "Не назначена" : task.Create_dttm;
            inpCloseDttm.Text = task.Planned_close_dt == System.Data.SqlTypes.SqlDateTime.MinValue.Value.ToString() ? "Не назначена" : task.Planned_close_dt;
            inpClose.Text = task.Close_dttm == null ? "Задача открыта" : task.Close_dttm;
        }

        void bEditClick(Object sender, EventArgs e)
        {
            Response.Redirect("EditingTask.aspx?Number=" + Number.ToString() + "&userId=" + Id.ToString());
        }
        void bBackClick(Object sender, EventArgs e)
        {
            if (PredPage == "MyTasks")
                Response.Redirect("TasksList.aspx?userId=" + Id.ToString());
            else if (PredPage == "SubusersTasks")
                Response.Redirect("SubuserTasksList.aspx?userId=" + Id.ToString());
        }

        private string GetTaskInfoStr()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/tasks/" + Number);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }

        private string GetUserInfoStr(int Id)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/users/" + Id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }
    }
}