﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditingTask.aspx.cs" Inherits="ToDoWebApp.EditingTask" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles.css" />
</head>
<body>
    <form id="rsvpform" runat="server">
        <div>
            <h1>Редактирование задачи</h1>
            <p>Поля со звездочкой не могут быть пустыми.</p>
        </div>
        <div class="reg_table">
            <div class="field">
                <label>Название*:</label>
                <asp:TextBox id="inpName" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Описание:</label>
                <asp:TextBox id="inpDesc" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Приоритет:</label>
                <asp:TextBox id="inpPrior" runat="server" CssClass="inp_element" TextMode="Number"/>
            </div>
            <br />
            <div class="field">
                <label>Оценка:</label>
                <asp:TextBox id="inpAppr" runat="server" CssClass="inp_element" TextMode="Number"/>
            </div>
            <br />
            <div class="field">
                <label>Исполнитель*:</label>
                <asp:DropDownList id="inpExec" runat="server" CssClass="inp_dropbox">

                </asp:DropDownList>
            </div>
            <br />
            <div class="field">
                <label>Статус:</label>
                <asp:DropDownList id="inpStatus" runat="server" CssClass="inp_dropbox">

                </asp:DropDownList>
            </div>
            <br />
            <div class="field">
                <label>Прогресс:</label>
                <asp:TextBox id="inpProgress" runat="server" CssClass="inp_element" TextMode="Number"/>
            </div>
            <br />
            <div class="field">
                <label>Планируемая дата начала:</label>
                <asp:TextBox id="inpStartDttm" runat="server" CssClass="inp_element" type="date"/>
            </div>
            <br />
            <div class="field">
                <label>Планируемая дата завершения:</label>
                <asp:TextBox id="inpCloseDttm" runat="server" CssClass="inp_element" type="date"/>
            </div>
            <br />
            <div class="field">
                <label>Закрыть задачу</label>
                <asp:CheckBox id="inpClose" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <asp:Label id="lError" runat="server" Text=" " ForeColor="Red"></asp:Label>
            <br />
            <div>
                <asp:Button ID="bSave" runat="server" Text="Сохранить"/>
                <asp:Button ID="bCancel" runat="server" Text="Назад"/>
            </div>
        </div>
    </form>
</body>
</html>
