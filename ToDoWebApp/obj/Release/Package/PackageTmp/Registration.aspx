﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="ToDoWebApp.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles.css" />
</head>
<body>
    <form id="rsvpform" runat="server">
        <div>
            <h1>Регистрация</h1>
            <p>Если вы пока не знаете своего начальника, оставьте данное поле пустым.<br />
                Остальные поля обязательны для заполнения.</p>
        </div>
        <div class="reg_table">
            <div class="field">
                <label>Логин:</label>
                <asp:TextBox id="inpLogin" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Фамилия:</label>
                <asp:TextBox id="inpLastname" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Имя:</label>
                <asp:TextBox id="inpFirstname" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Отчество:</label>
                <asp:TextBox id="inpMiddlename" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Должность:</label>
                <asp:DropDownList id="inpRole" runat="server" CssClass="inp_dropbox">

                </asp:DropDownList>
            </div>
            <br />
            <div class="field">
                <label>Начальник:</label>
                <asp:DropDownList id="inpBoss" runat="server" CssClass="inp_dropbox">

                </asp:DropDownList>
            </div>
            <br />
            <div class="field">
                <label>Пароль:</label>
                <asp:TextBox id="inpPassword" TextMode="Password" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <div class="field">
                <label>Повторите пароль:</label>
                <asp:TextBox id="inpConfirmPassword" TextMode="Password" runat="server" CssClass="inp_element"/>
            </div>
            <br />
            <asp:Label id="lError" runat="server" Text=" " ForeColor="Red"></asp:Label>
            <br />
            <div>
                <asp:Button ID="bRegistr" runat="server" Text="Зарегистрироваться"/>
                <asp:Button ID="bEnter" runat="server" Text="Есть аккаунт"/>
            </div>
        </div>
    </form>
</body>
</html>
