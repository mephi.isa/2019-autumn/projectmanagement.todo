﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Auth.aspx.cs" Inherits="ToDoWebApp.Auth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles.css" />
</head>
<body>
    <form id="rsvpform" runat="server">
        <div>
            <h1>Вход</h1>
            <p>Введите логин и пароль, чтобы увидеть свой список задач.</p>
        </div>
        <div>
            <label>Логин:</label>
            <br />
            <asp:TextBox id="inpLogin" runat="server" />
        </div>
        <br />
        <div>
            <label>Пароль:</label>
            <br />
            <asp:TextBox id="inpPassword" TextMode="Password" runat="server" />
        </div>
            <asp:Label id="lError" runat="server" Text=" " ForeColor="Red"></asp:Label>
        <br />
        <div>
            <asp:Button ID="bEnter" runat="server" Text="Войти"/>
            <asp:Button ID="bRegistr" runat="server" Text="Зарегистрироваться"/>
        </div>
    </form>
</body>
</html>
