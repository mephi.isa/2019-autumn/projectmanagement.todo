﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ToDoListNamespace;

namespace ToDoWebApp
{
    public partial class AddingTask : System.Web.UI.Page
    {
        string Id;
        protected void Page_Load(object sender, EventArgs e)
        {
            Id = Request.QueryString["userId"];

            if (!Page.IsPostBack)
            {
                List<string> Fios = new List<string> { "" };
                List<UserInfo> subusers = GetSubusers();

                string json = GetUserInfoStr();
                UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(json);

                Fios.Add(user.FIO);
                foreach (var subuser in subusers)
                    Fios.Add(subuser.FIO);

                inpExec.DataSource = Fios;
                inpExec.DataBind();

                List<string> statuses = new List<string> { "Активно", "Неактивно" };
                inpStatus.DataSource = statuses;
                inpStatus.DataBind();
            }

            bSave.Click += bSaveClick;
            bCancel.Click += bCancelClick;
        }

        void bSaveClick(Object sender, EventArgs e)
        {
            if (inpName.Text.Trim() == "")
            {
                lError.Text = "Не введено название задачи!";
                return;
            }

            string JsonRes = SendAddRequest();
            if (JsonRes.Contains("Ошибка"))
                lError.Text = JsonRes;
            else if (int.Parse(JsonRes) != 0)
                lError.Text = "Ошибка добавления задачи: код " + int.Parse(JsonRes);
            else
            {
                Response.Redirect("TasksList.aspx?userId=" + Id.ToString());
            }
        }

        void bCancelClick(Object sender, EventArgs e)
        {
            Response.Redirect("TasksList.aspx?userId=" + Id.ToString());
        }

        private string SendAddRequest()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/tasks");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string status = "active";
                switch (inpStatus.Text)
                {
                    case "Активно":
                        status = "active";
                        break;
                    case "Неактивно":
                        status = "inactive";
                        break;
                    default:
                        status = "";
                        break;
                }

                int prior = -1, appr = -1;
                if (inpPrior.Text != "")
                    if (!int.TryParse(inpPrior.Text, out prior))
                        return "Ошибка! Неверно заполнено поле \"Приоритет\"";

                if (inpAppr.Text != "")
                    if (!int.TryParse(inpAppr.Text, out appr))
                        return "Ошибка! Неверно заполнено поле \"Оценка\"";

                int ExecId = -1;
                if (inpExec.Text == "")
                    return "Ошибка! Незаполнено поле \"Исполнитель\"";
                else
                {
                    List<UserInfo> execs = GetSubusers();
                    string jsonUserStr = GetUserInfoStr();
                    UserInfo user = new JavaScriptSerializer().Deserialize<UserInfo>(jsonUserStr);

                    execs.Add(user);
                    UserInfo FindUser = execs.FirstOrDefault(a => a.FIO == inpExec.Text);
                    if (FindUser == null)
                        return "Ошибка! Исполнитель не найден";
                    ExecId = FindUser.Id;
                }

                DateTime test;
                string json = new JavaScriptSerializer().Serialize(new
                {
                    WhoId = int.Parse(Id), 
                    Name = inpName.Text,
                    Description = inpDesc.Text,
                    Priority = prior,
                    ExecutorId = ExecId,
                    Apprasial = appr,
                    Progress = 0,
                    Status = status,
                    Assign_dttm = DateTime.Now.ToString(),
                    Planned_start = DateTime.TryParse(inpStartDttm.Text, out test) ? inpStartDttm.Text : "",
                    Planned_close = DateTime.TryParse(inpCloseDttm.Text, out test) ? inpCloseDttm.Text : "",
                });
                streamWriter.Write(json);
            }

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
            return result;
        }

        private List<UserInfo> GetSubusers()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/subusers/" + Id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();
            List<UserInfo> users = new JavaScriptSerializer().Deserialize<List<UserInfo>>(result);

            return users;
        }

        private string GetUserInfoStr()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Controller.Path + "api/users/" + Id);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            string result;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            httpResponse.Close();

            return result;
        }
    }
}