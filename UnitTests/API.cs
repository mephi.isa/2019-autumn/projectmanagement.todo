﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListNamespace
{
    public class UserInfo
    {
        public int Id { get; set; }
        public string FIO { get; set; }
        public string Role { get; set; }
        public int ManagerId { get; set; }

        public UserInfo(int id, string fio, string role, int managerId)
        {
            Id = id;
            FIO = fio;
            Role = role;
            ManagerId = managerId;
        }

        public UserInfo()
        {

        }
    }

    public class TaskInfo
    {
        public int TaskNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatorId { get; set; }
        public int Priority { get; set; }
        public int ExecutorId { get; set; }
        public int Apprasial { get; set; }
        public string Status { get; set; }
        public string Create_dttm { get; set; }
        public string Assign_dttm { get; set; }
        public string Planned_start_dt { get; set; }
        public string Planned_close_dt { get; set; }
        public string Close_dttm { get; set; }
        public int Above { get; set; }
        public int Progress { get; set; }

        public TaskInfo(string[] param)
        {
            TaskNumber = int.Parse(param[0]);
            Name = param[1];
            Description = param[2];
            CreatorId = int.Parse(param[3]);
            Priority = int.Parse(param[4]);
            ExecutorId = int.Parse(param[5]);
            Apprasial = int.Parse(param[6]);
            Status = param[7];
            Create_dttm = param[8];
            Assign_dttm = param[9];
            Planned_start_dt = param[10];
            Planned_close_dt = param[11];
            Close_dttm = param[12];
            Above = int.Parse(param[13]);
            Progress = int.Parse(param[14]);
        }

        public TaskInfo()
        {

        }
    }

	public class API
	{
		public IDatabaseController database;
        public IRegistrationDatabase registr;

		// Соединения с БД
		public int ConnectDatabase(bool TestDatabase = false)
		{
            if (TestDatabase)
                database = new DatabaseController();
            else
			    database = new SqlDatabaseController();
			return 0;
		}

        // Соединения с системой регистрации/авторизации
        public int ConnectRegistrationDatabase(bool TestDatabase = false)
        {
            if (TestDatabase)
                registr = new Registration();
            else
                registr = new RegistrationJson();
            return 0;
        }

        // Возвращает список подчиненных
        public List<UserInfo> GetSubusersInfo(int managerId)
        {
            if (GetUserInfoById(managerId).Role != "Manager")
                return null;

            List<User> subUsers = database.GetSubusers(managerId);
            List<UserInfo> SubusersInfo = new List<UserInfo>();

            foreach (User user in subUsers)
            {
                SubusersInfo.Add(new UserInfo(user.Id, user.GetFullName(), user.Role, user.Manager.Id));
            }

            return SubusersInfo;
        }

        // Возвращает список задач пользователя
        public List<TaskInfo> GetUserTasksInfo(int userId)
		{
            if (database.GetUserById(userId) == null)
                return null;

            List<TaskInfo> tasksInfo = new List<TaskInfo>();

            List<Task> tasks = database.GetUserTasks(userId);
            foreach (Task task in tasks)
            {
                tasksInfo.Add(new TaskInfo(task.GetFullInfo()));
            }

            return tasksInfo;
        }

        // Возвращает список задач подчиненных пользователя
        public Dictionary<int, List<TaskInfo>> GetSubuserTasksInfo(int managerId)
        {
            if (GetUserInfoById(managerId).Role != "Manager")
                return null;

            Dictionary<int, List<TaskInfo>> DictSubsuserTasks = new Dictionary<int, List<TaskInfo>>();

            List<User> use = database.GetUsers();
            List<User> users = database.GetSubusers(managerId);

            foreach (User user in users)
            {
                DictSubsuserTasks.Add(user.Id, GetUserTasksInfo(user.Id));
            }

            return DictSubsuserTasks;
        }

		// Создание задачи
		public int CreateTask(string name, int creatorId, string description = "", int priority = -1, int? executorId = null, double apprasial = -1, int progress = 0,
            string status = "", string assign_dttm = "", string planned_start_dt = "", string planned_close_dt = "")
		{
            User creator = database.GetUserById(creatorId);
            if (creator.Role != "Manager")
                return -1;

            if (executorId != null)
                if (database.GetUserById(executorId) == null)
                    return -4;

            Task task;
            DateTime assig;
            try
            {
                task = new Task(database.GenerateTaskNum(), name, creator, database.GenerateTaskNum(), description, priority, database.GetUserById(executorId), apprasial, progress, status,
                    DateTime.TryParse(assign_dttm, out assig) ? assig : DateTime.Now, planned_start_dt == "" ? System.Data.SqlTypes.SqlDateTime.MinValue.Value : DateTime.Parse(planned_start_dt),
                    planned_close_dt == "" ? System.Data.SqlTypes.SqlDateTime.MinValue.Value : DateTime.Parse(planned_close_dt));
            }
            catch(Exception e)
            {
                return -6;
            }

			if (task.Name == null)
				return -7;

            // сохранение в БД
            return database.AddTask(task);
		}

        // Редактирование задачи
        public int EditTask(int userId, int number, string name, string description, int priority, int? executorId, double apprasial, int progress, string status, string assign_dttm, string planned_start_dt, string planned_close_dt)
        {
            Task task = database.GetTaskByNum(number);

            if (task == null)
                return 1;

            //if (userId != task.Creator.Id)
            //    if (task.Executor == null)
            //        return -1;
            //    else if (userId != task.Executor.Id)
            //        return -1;

            if (executorId != null)
                if (database.GetUserById(executorId) == null)
                    return -4;

            int Result;
            DateTime assig;
            try
            {
                Result = task.Update(name, description, priority, database.GetUserById(executorId), apprasial, progress, status,
                    DateTime.TryParse(assign_dttm, out assig) ? assig : DateTime.Now,
                    planned_start_dt == "" ? System.Data.SqlTypes.SqlDateTime.MinValue.Value : DateTime.Parse(planned_start_dt),
                    planned_close_dt == "" ? System.Data.SqlTypes.SqlDateTime.MinValue.Value : DateTime.Parse(planned_close_dt));
            }
            catch(Exception e)
            {
                return -6;
            }

            if (Result != 0)
                return Result;

            return database.EditTask(task);
        }

        public int CloseTask(int TaskNum)
        {
            Task task = database.GetTaskByNum(TaskNum);

            task.Close();

            return database.EditTask(task);
        }

        // Получение данных пользователя по Id
        public UserInfo GetUserInfoById(int id)
        {
            User user = database.GetUserById(id);

            if (user == null)
                return null;

            return new UserInfo(user.Id, user.GetFullName(), user.Role, user.Manager == null ? -1 : user.Manager.Id);
        }

        public TaskInfo GetTaskInfoByNum(int num)
        {
            Task task = database.GetTaskByNum(num);

            if (task == null)
                return null;

            return new TaskInfo(task.GetFullInfo());
        }

        // Авторизация
        // Возвращает id пользователя, если данные верны и -1 в противном случае
        public int Authorizate(string login, string password)
        {
            List<UserLogin> userLogins = registr.GetLoginPasswordDatabase();

            string st = GetHash(password);
            UserLogin user = userLogins.FirstOrDefault(a => a.Login == login && a.Password == GetHash(password));
            if (user != null)
                return user.Id;

            return -1;
        }

        // Регистрация
        // Возвращает id нового пользователя, если регистрация успешна и -1 в противном случае
        public int Registration(string login, string password, string Last_name, string First_name, string Middle_name, string Role, int? ManagerId = null)
        {
            // Проверка логина и пароля
            if (!registr.CheckCorrectLogin(login))
                return -1;

            // Запись логина и пароля в БД
            int id = registr.AddNewUserLogin(login, password);

            // получение руководителя из БД по Id
            User Manager = database.GetUserById(ManagerId);

            User user;
            user = new User(Last_name, First_name, Middle_name, Role, Manager, id);

            // сохранение в БД
            int AddDatabaseResult = database.AddUser(user);
            if (AddDatabaseResult != 0)
                return -1;

            return id;
        }

        public string GetHash(string str)
        {
            return registr.getHash(str);
        }

        public List<UserInfo> GetUsersInfo()
        {
            List<User> users = database.GetUsers();
            List<UserInfo> usersInfo = new List<UserInfo>();

            foreach(User user in users)
            {
                usersInfo.Add(new UserInfo(user.Id, user.GetFullName(), user.Role, user.Manager == null ? -1 : user.Manager.Id));
            }

            return usersInfo;
        }

        public List<TaskInfo> GetTasksInfo()
        {
            List<Task> tasks = database.LoadTasks();
            List<TaskInfo> tasksInfo = new List<TaskInfo>();

            foreach (Task task in tasks)
            {
                tasksInfo.Add(new TaskInfo(task.GetFullInfo()));
            }

            return tasksInfo;
        }

        public void SwapTasks(int num1, int num2)
        {
            database.Swap(num1, num2);
        }
    }
}
