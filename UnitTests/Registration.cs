﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace ToDoListNamespace
{
    public class UserLogin
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public UserLogin(int id, string login, string password)
        {
            Id = id;
            Login = login;
            Password = password;
        }

        public UserLogin()
        {

        }
    }
    public class Registration: IRegistrationDatabase
    {
        public List<UserLogin> userLogins;
        public Registration(bool Test = true)
        {
            userLogins = new List<UserLogin>();
            if (Test)
            {
                userLogins.Add(new UserLogin(6, "admin", getHash("password")));
                userLogins.Add(new UserLogin(1, "petrov", getHash("pet123")));
                userLogins.Add(new UserLogin(0, "ivanov", getHash("iv456")));
            }
        }

        public string getHash(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        public List<UserLogin> GetLoginPasswordDatabase()
        {
            return userLogins;
        }

        public int AddNewUserLogin(string login, string password)
        {
            int id = GenerateNewId();
            userLogins.Add(new UserLogin(id, login, getHash(password)));

            return id;
        }

        public int GenerateNewId()
        {
            if (userLogins.Count == 0)
                return 0;
            return userLogins.Max(b => b.Id) + 1;
        }

        // Проверяет, доступен ли такой логин
        public bool CheckCorrectLogin(string login)
        {
            return userLogins.FirstOrDefault(a => a.Login == login) == null;
        }
    }
}
