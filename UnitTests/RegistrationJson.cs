﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace ToDoListNamespace
{
    class RegistrationJson: IRegistrationDatabase
    {
        string Path = "D:\\ДОКУМЕНТЫ ОБЩИЕ\\НИКИТА\\Лабы\\Магистратура\\АИС\\ToDo\\projectmanagement.todo\\registr.json";
        List<UserLogin> UsLogins = new List<UserLogin>();
        public static bool Test = false;

        public RegistrationJson()
        {
            if (Test)
            {
                Path = "D:\\ДОКУМЕНТЫ ОБЩИЕ\\НИКИТА\\Лабы\\Магистратура\\АИС\\ToDo\\projectmanagement.todo\\registrTest.json";
                File.WriteAllText(Path, "[]");

                List<UserLogin> UsLogins = new List<UserLogin>();
                AddNewUserLogin("ivanov", "iv456");
                AddNewUserLogin("petrov", "pet123");
                AddNewUserLogin("sidorov", "12345");
                AddNewUserLogin("gordeev", "12345");
                AddNewUserLogin("admin", "password");
            }
        }

        public List<UserLogin> GetLoginPasswordDatabase()
        {
            string json;

            using (FileStream fstream = new FileStream(Path, FileMode.Open))
            {
                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                json = System.Text.Encoding.Default.GetString(array);
            }

            UsLogins = JsonSerializer.Deserialize<List<UserLogin>>(json);
            return UsLogins;
        }

        public int AddNewUserLogin(string login, string password)
        {
            int id = GenerateNewId();
            UsLogins = GetLoginPasswordDatabase();
            UsLogins.Add(new UserLogin(id, login, getHash(password)));

            using (FileStream fstream = new FileStream(Path, FileMode.OpenOrCreate))
            {
                string database = JsonSerializer.Serialize<List<UserLogin>>(UsLogins);
                byte[] array = System.Text.Encoding.Default.GetBytes(database);
                fstream.Write(array, 0, array.Length);
            }

            return id;
        }

        public int GenerateNewId()
        {
            if (UsLogins.Count == 0)
                return 0;
            return UsLogins.Max(a => a.Id) + 1;
        }

        public bool CheckCorrectLogin(string login)
        {
            UsLogins = GetLoginPasswordDatabase();
            return UsLogins.FirstOrDefault(a => a.Login == login) == null;
        }

        public string getHash(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
    }
}
