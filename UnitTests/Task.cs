﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListNamespace
{
	public class Task
	{
		public int TaskNumber { get; set; }
		public string Name { get; set; }
        public string Description { get; set; }
        public User Creator { get; set; }
		public int Priority { get; set; }
        public User Executor { get; set; }
        public double Apprasial { get; set; }
        public string Status { get; set; }
        public DateTime Create_dttm { get; set; }
        public DateTime Assign_dttm { get; set; }
        public DateTime Planned_start_dt { get; set; }
        public DateTime Planned_close_dt { get; set; }
        private DateTime? close_dttm;
		public DateTime? Close_dttm 
        { 
            get
            {
                return close_dttm;
            }
            set
            {
                if (value > Create_dttm)
                    close_dttm = value;
            }
        }
		public int Above { get; set; }
		public int Progress { get; set; }

        public Task()
        {

        }

        public Task(int number, string name, User creator, int above, string description = "", int? priority = null, User executor = null, double? apprasial = null, int? progress = null, string status = "",
            DateTime? assign_dttm = null, DateTime? planned_start_dt = null, DateTime? planned_close_dt = null, DateTime? closed_dttm = null)
		{
            if (creator == null)
                return;

            if (Update(name, description, priority, executor, apprasial, progress, status, assign_dttm, planned_start_dt, planned_close_dt, closed_dttm) != 0)
                return;

            TaskNumber = number;
            Creator = creator;
            Above = above;
		}

        public int Update(string name, string description, int? priority, User executor, double? apprasial, int? progress, string status, DateTime? assign_dttm, DateTime? planned_start_dt, DateTime? planned_close_dt, DateTime? closed_dttm = null)
        {
            if (name == "")
                return 1;
            if (planned_close_dt < planned_start_dt)
                if (planned_close_dt != System.Data.SqlTypes.SqlDateTime.MinValue.Value)
                    return 1;
            if (progress < 0 || progress > 100)
                return 1;
            if (status != "active" && status != "inactive" && status != "fail" && status != "done" && status != "")
                return 1;

            Name = name;
            Close_dttm = closed_dttm;
            Create_dttm = DateTime.Now;

            Description = description;
            Priority = (priority == null) ? -1 : priority.Value;
            Executor = executor;
            Apprasial = (apprasial == null) ? -1 : apprasial.Value;
            Progress = (progress == null) ? 0 : progress.Value;
            Status = status;
            Assign_dttm = (assign_dttm == null) ? System.Data.SqlTypes.SqlDateTime.MinValue.Value : assign_dttm.Value;
            Planned_start_dt = (planned_start_dt == null) ? System.Data.SqlTypes.SqlDateTime.MinValue.Value : planned_start_dt.Value;
            Planned_close_dt = (planned_close_dt == null) ? System.Data.SqlTypes.SqlDateTime.MinValue.Value : planned_close_dt.Value;

            return 0;
        }

		 public string[] GetFullInfo()
		{
			string[] prop_list = new string[] {
			TaskNumber.ToString(),
            Name,
            Description,
            Creator.Id.ToString(),
            Priority.ToString(),
            Executor == null ? "-1" : Executor.Id.ToString(),
            Apprasial.ToString(),
            Status,
            Create_dttm.ToString(),
            Assign_dttm.ToString(),
            Planned_start_dt.ToString(),
            Planned_close_dt.ToString(),
            Close_dttm?.ToString(),
            Above.ToString(),
            Progress.ToString()
            };

			return prop_list;
		}

		public void Assign(User executor)
		{
			Executor = executor;
			Assign_dttm = DateTime.Now;
		}

		public int Close()
		{
			if (close_dttm != null)
				return 1;

			Close_dttm = DateTime.Now;
			return 0;
		}

		public bool isClosed()
		{
			return Close_dttm != null;
		}

		public bool isCompleted()
		{
			return Progress == 100;
		}
	}
}
