﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListNamespace
{
	public class DatabaseController: IDatabaseController
	{
		// Здесь будет соединение с базой данных и прочее

		public List<User> Users;
		public List<Task> Tasks;

        public DatabaseController(bool TestData = true)
        {
            Users = new List<User>();
            Tasks = new List<Task>();

            if (TestData)
            {
                Users.Add(new User("Admin", "Admin", "Admin", "Admin", null, 0));
                Users.Add(new User("Петров", "Петр", "Петрович", "Manager", null, 1));
                Users.Add(new User("Иванов", "Иван", "Иванович", "Employee", Users[1], 2));
                Users.Add(new User("Сидоров", "Никита", "Степанович", "Employee", Users[1], 3));
                Users.Add(new User("Гордеев", "Александр", "Александрович", "Employee", null, 4));

                Tasks.Add(new Task(1, "task0", Users[0], 0, "task_description0", 2, Users[1], 5, 0, "active", new DateTime(2019, 11, 16), new DateTime(2019, 11, 17), new DateTime(2019, 12, 21)));
                Tasks.Add(new Task(2, "task1", Users[0], 1, "task_description1", 2, Users[2], 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18)));
                Tasks.Add(new Task(3, "task2", Users[0], 2, "task_description2", 2, Users[1], 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18)));
                Tasks.Add(new Task(4, "task3", Users[3], 3, "task_description3", 2, Users[3], 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18)));
            }
        }

		public List<Task> LoadTasks()
		{
			return Tasks;
		}

		public List<User> GetUsers()
		{
			return Users;
		}

		public List<User> GetSubusers(int managerId)
		{
			List<User> users = GetUsers();

			return users.Where(a => a.Manager?.Id == managerId).ToList();
		}

		public List<Task> GetUserTasks(int userId)
		{
			List<Task> tasks = LoadTasks();

			return tasks.Where(a => a.Executor.Id == userId).ToList();
		}


        public int AddTask(Task task)
        {
            if (Tasks.FirstOrDefault(a => a.TaskNumber == task.TaskNumber) == null)
            {
                Tasks.Add(task);
                return 0;
            }

            return 1;
        }

        public int EditTask(Task task)
        {
            Tasks.Remove(Tasks.FirstOrDefault(a => a.TaskNumber == task.TaskNumber));
            Tasks.Add(task);
            return 0;
        }

        public int GenerateTaskNum()
        {
            if (Tasks.Count == 0)
                return 0;
            return Tasks.Max(a => a.TaskNumber) + 1;
        }

        public int AddUser(User user)
        {
            if (Users.FirstOrDefault(a => a.Id == user.Id) == null)
            {
                Users.Add(user);
                return 0;
            }

            return 1;
        }

        public User GetUserById(int? id)
        {
            if (id == null)
                return null;

            return Users.FirstOrDefault(a => a.Id == id);
        }

        public Task GetTaskByNum(int? num)
        {
            if (num == null)
                return null;

            return Tasks.FirstOrDefault(a => a.TaskNumber == num);
        }

        public void Swap(int num1, int num2)
        {
            Task task1 = Tasks.FirstOrDefault(a => a.Above == num1);
            Task task2 = Tasks.FirstOrDefault(a => a.Above == num2);
            task1.Above = num2;
            task2.Above = num1;
        }
    }
}
