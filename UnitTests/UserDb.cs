﻿using System;
using System.Collections.Generic;

namespace ToDoListNamespace
{
    public partial class UserDb
    {
        public UserDb()
        {
            InverseManagerNavigation = new HashSet<UserDb>();
            TaskDbCreatorNavigation = new HashSet<TaskDb>();
            TaskDbExecutorNavigation = new HashSet<TaskDb>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public int? Manager { get; set; }

        public virtual UserDb ManagerNavigation { get; set; }
        public virtual ICollection<UserDb> InverseManagerNavigation { get; set; }
        public virtual ICollection<TaskDb> TaskDbCreatorNavigation { get; set; }
        public virtual ICollection<TaskDb> TaskDbExecutorNavigation { get; set; }
    }
}
