﻿using System;
using System.Collections.Generic;

namespace ToDoListNamespace
{
    public partial class TaskDb
    {
        public TaskDb()
        {
            InverseTaskAboveNavigation = new HashSet<TaskDb>();
            InverseTaskBelowNavigation = new HashSet<TaskDb>();
        }

        public int TaskNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Creator { get; set; }
        public int? Priority { get; set; }
        public int? Executor { get; set; }
        public double? Apprasial { get; set; }
        public string Status { get; set; }
        public DateTime CreateDttm { get; set; }
        public DateTime? AssignDttm { get; set; }
        public DateTime? PlannedStartDt { get; set; }
        public DateTime? PlannedCloseDt { get; set; }
        public DateTime? CloseDttm { get; set; }
        public int? TaskAbove { get; set; }
        public int? TaskBelow { get; set; }
        public int? Progress { get; set; }

        public virtual UserDb CreatorNavigation { get; set; }
        public virtual UserDb ExecutorNavigation { get; set; }
        public virtual TaskDb TaskAboveNavigation { get; set; }
        public virtual TaskDb TaskBelowNavigation { get; set; }
        public virtual ICollection<TaskDb> InverseTaskAboveNavigation { get; set; }
        public virtual ICollection<TaskDb> InverseTaskBelowNavigation { get; set; }
    }
}
