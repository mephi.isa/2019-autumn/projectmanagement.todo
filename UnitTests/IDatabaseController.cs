﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListNamespace
{
    public interface IDatabaseController
    {
        // Загрузить список задач
        List<Task> LoadTasks();

        // Загрузить список пользователей
        List<User> GetUsers();

        // Загрузить список подчиненных пользователя
        List<User> GetSubusers(int managerId);

        // Загрузить список задач пользователя
        List<Task> GetUserTasks(int userId);

        // Добавить задачу
        int AddTask(Task task);

        // Редактировать задачу
        int EditTask(Task task);

        // Сгенерировать номер задачи
        int GenerateTaskNum();

        // Добавить пользователя
        int AddUser(User user);

        // Получить пользователя по Id
        User GetUserById(int? id);

        // Получить задачу по номеру
        Task GetTaskByNum(int? num);

        //Поменять задачи местами
        void Swap(int num1, int num2);
    }

}
