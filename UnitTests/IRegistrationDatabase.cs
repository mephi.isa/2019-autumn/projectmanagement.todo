﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListNamespace
{
    public interface IRegistrationDatabase
    {
        List<UserLogin> GetLoginPasswordDatabase();

        int AddNewUserLogin(string login, string password);

        int GenerateNewId();

        bool CheckCorrectLogin(string login);

        string getHash(string text);
    }
}

