﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListNamespace
{
    public class SqlDatabaseController : IDatabaseController
    {
        UsersTasksDBContext database;
        List<User> Users = new List<User>();
        List<Task> Tasks = new List<Task>();
        private static bool Test = false;

        public SqlDatabaseController()
        {
            database = new UsersTasksDBContext(Test);

            if (Test)
                TestData();

            Users = GetUsers();
            Tasks = LoadTasks();
        }

        private void RemoveAll()
        {
            foreach(TaskDb task in database.TaskDb)
            {
                task.TaskAbove = null;
                task.TaskBelow = null;
            }
            database.SaveChanges();
            database.TaskDb.RemoveRange(database.TaskDb);
            database.UserDb.RemoveRange(database.UserDb);
            database.SaveChanges();
        }

        private void TestData()
        {
            RemoveAll();

            AddUser(new User("Admin", "Admin", "Admin", "Admin", null, 0));
            AddUser(new User("Петров", "Петр", "Петрович", "Manager", null, 1));
            AddUser(new User("Иванов", "Иван", "Иванович", "Employee", GetUserById(1), 2));
            AddUser(new User("Сидоров", "Никита", "Степанович", "Employee", GetUserById(1), 3));
            AddUser(new User("Гордеев", "Александр", "Александрович", "Employee", null, 4));

            AddTask(new Task(1, "task0", GetUserById(0), 0, "task_description0", 2, GetUserById(1), 5, 0, "active", new DateTime(2019, 11, 16), new DateTime(2019, 11, 17), new DateTime(2019, 12, 21)));
            AddTask(new Task(2, "task1", GetUserById(0), 1, "task_description1", 2, GetUserById(2), 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18)));
            AddTask(new Task(3, "task2", GetUserById(0), 2, "task_description2", 2, GetUserById(1), 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18)));
            AddTask(new Task(4, "task3", GetUserById(0), 3, "task_description3", 2, GetUserById(3), 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18)));
            database.TaskDb.FirstOrDefault(a => a.TaskNumber == 1).TaskAbove = 3;
            database.SaveChanges();
        }

        public int AddTask(Task task)
        {
            if (database.TaskDb.FirstOrDefault(a => a.TaskNumber == task.TaskNumber) == null)
            {
                TaskDb taskDB = new TaskDb()
                {
                    TaskNumber = task.TaskNumber,
                    Name = task.Name,
                    Creator = task.Creator.Id,
                    Description = task.Description,
                    Executor = task.Executor?.Id,
                    Priority = task.Priority,
                    Apprasial = task.Apprasial,
                    Progress = task.Progress,
                    Status = task.Status,
                    AssignDttm = task.Assign_dttm,
                    PlannedStartDt = task.Planned_start_dt,
                    PlannedCloseDt = task.Planned_close_dt,
                    TaskAbove = task.Above,
                    CreateDttm = task.Create_dttm,
                    CloseDttm = task.Close_dttm
                };

                database.TaskDb.Add(taskDB);
                database.SaveChanges();
                Tasks = LoadTasks();
                return 0;
            }

            return 1;
        }

        public int AddUser(User user)
        {
            if (database.UserDb.FirstOrDefault(a => a.Id == user.Id) == null)
            {
                UserDb userDB = new UserDb()
                {
                    Id = user.Id,
                    FirstName = user.First_name,
                    MiddleName = user.Middle_name,
                    LastName = user.Last_name,
                    Role = user.Role,
                    Manager = user.Manager?.Id
                };
                database.UserDb.Add(userDB);
                database.SaveChanges();
                Users = GetUsers();
                return 0;
            }

            return 1;
        }

        public int EditTask(Task task)
        {
            database.TaskDb.Remove(database.TaskDb.FirstOrDefault(a => a.TaskNumber == task.TaskNumber));
            database.SaveChanges();
            TaskDb taskDB = new TaskDb()
            {
                TaskNumber = task.TaskNumber,
                Name = task.Name,
                Creator = task.Creator.Id,
                Description = task.Description,
                Executor = task.Executor?.Id,
                Priority = task.Priority,
                Apprasial = task.Apprasial,
                Progress = task.Progress,
                Status = task.Status,
                AssignDttm = task.Assign_dttm,
                PlannedStartDt = task.Planned_start_dt,
                PlannedCloseDt = task.Planned_close_dt,
                TaskAbove = task.Above,
                CreateDttm = task.Create_dttm,
                CloseDttm = task.Close_dttm
            };

            database.TaskDb.Add(taskDB);
            database.SaveChanges();
            Tasks = LoadTasks();
            return 0;
        }

        public int GenerateTaskNum()
        {
            if (Tasks.Count() == 0)
                return 0;
            return Tasks.Max(a => a.TaskNumber) + 1;
        }

        public List<User> GetSubusers(int managerId)
        {
            Users = GetUsers();
            return Users.Where(a => a.Manager?.Id == managerId).ToList();
        }

        public Task GetTaskByNum(int? num)
        {
            if (num == null)
                return null;

            return Tasks.FirstOrDefault(a => a.TaskNumber == num);
        }

        public User GetUserById(int? id)
        {
            if (id == null)
                return null;

            return Users.FirstOrDefault(a => a.Id == id);
        }

        public List<User> GetUsers()
        {
            List<UserDb> UsersDB = database.UserDb.ToList();
            List<User> Users = new List<User>();

            foreach(UserDb userDB in UsersDB)
            {
                Users.Add(new User(userDB.LastName, userDB.FirstName, userDB.MiddleName, userDB.Role, GetUserById(userDB?.Manager), userDB.Id));
            }

            return Users;
        }

        public List<Task> GetUserTasks(int userId)
        {
            Tasks = LoadTasks();
            return Tasks.Where(a => a.Executor.Id == userId).ToList();
        }

        public List<Task> LoadTasks()
        {
            List<TaskDb> TasksDB = database.TaskDb.ToList();
            List<Task> Tasks = new List<Task>();

            foreach(TaskDb taskDB in TasksDB)
            {
                Tasks.Add(new Task(taskDB.TaskNumber, taskDB.Name, GetUserById(taskDB.Creator), taskDB.TaskAbove.Value, taskDB.Description, taskDB.Priority, GetUserById(taskDB.Executor), taskDB.Apprasial, taskDB.Progress, taskDB.Status,
                taskDB.AssignDttm, taskDB.PlannedStartDt, taskDB.PlannedCloseDt, taskDB.CloseDttm));
            }

            return Tasks;
        }

        public void Swap(int num1, int num2)
        {
            TaskDb task1 = database.TaskDb.FirstOrDefault(a => a.TaskAbove == num1);
            TaskDb task2 = database.TaskDb.FirstOrDefault(a => a.TaskAbove == num2);
            task1.TaskAbove = num2;
            task2.TaskAbove = num1;
            database.SaveChanges();
        }
    }
}
