﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListNamespace
{
	public class User
	{
		public int Id { get; set; }
		public string First_name { get; set; }
		public string Middle_name { get; set; }
		public string Last_name { get; set; }
		public string Role { get; set; }
		public User Manager { get; set; }

		public User()
		{

		}

		public User(string last, string first, string middle, string role, User manager, int id = -1)
		{
			First_name = first;
			Middle_name = middle;
			Last_name = last;
			Role = role;
			Manager = manager;

            Id = id;
		}

		public string GetFullName()
		{
			return string.Format("{0} {1} {2}", Last_name, First_name, Middle_name);
		}
	}
}
