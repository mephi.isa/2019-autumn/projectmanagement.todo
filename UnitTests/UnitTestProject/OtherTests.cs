﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ToDoListNamespace;
using CurDatabaseController = ToDoListNamespace.DatabaseController;

namespace UnitTestProject
{
    [TestClass]
    public class OtherTests
    {
        [TestMethod]
        public void TestUserConstructor()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            User user = new User("Иванов", "Иван", "Иванович", "Employee", manager, 0);
            Assert.AreEqual(0, user.Id);
            Assert.AreEqual("Иванов", user.Last_name);
            Assert.AreEqual("Иван", user.First_name);
            Assert.AreEqual("Иванович", user.Middle_name);
            Assert.AreEqual("Employee", user.Role);
            Assert.AreEqual(manager, user.Manager);
        }

        [TestMethod]
        public void TestUserGetFullName()
        {
            User user = new User("Иванов", "Иван", "Иванович", "Employee", null, 0);
            Assert.AreEqual("Иванов Иван Иванович", user.GetFullName());
        }

        [TestMethod]
        public void TestTaskShortConstructor()
        {
            User user = new User("Иванов", "Иван", "Иванович", "Employee", null, 0);
            Task task = new Task(1, "task1", user, 0);
            Assert.AreEqual(1, task.TaskNumber);
            Assert.AreEqual("task1", task.Name);
            Assert.AreEqual(user, task.Creator);
            Assert.AreEqual(0, task.Above);
        }

        [TestMethod]
        public void TestTaskFullConstructor()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            User user = new User("Иванов", "Иван", "Иванович", "Employee", manager, 0);
            Task task = new Task(1, "task1", manager, 0, "task_description", 2, user, 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18));
            Assert.AreEqual(1, task.TaskNumber);
            Assert.AreEqual("task1", task.Name);
            Assert.AreEqual(manager, task.Creator);
            Assert.AreEqual("task_description", task.Description);
            Assert.AreEqual(2, task.Priority);
            Assert.AreEqual(user, task.Executor);
            Assert.AreEqual(5, task.Apprasial);
            Assert.AreEqual(0, task.Progress);
            Assert.AreEqual("active", task.Status);
            Assert.AreEqual(new DateTime(2019, 11, 14), task.Assign_dttm);
            Assert.AreEqual(new DateTime(2019, 11, 15), task.Planned_start_dt);
            Assert.AreEqual(new DateTime(2019, 12, 18), task.Planned_close_dt);
        }

        [TestMethod]
        public void TestTaskFullConstructorFailDate()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            User user = new User("Иванов", "Иван", "Иванович", "Employee", manager, 0);
            Task task = new Task(1, "task1", manager, 0, "task_description", 2, user, 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2017, 12, 18));

            Assert.IsNull(task.Name);
        }

        [TestMethod]
        public void TestTaskFullConstructorFailStatus()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            User user = new User("Иванов", "Иван", "Иванович", "Employee", manager, 0);
            Task task = new Task(1, "task1", manager, 0, "task_description", 2, user, 5, 0, "1", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18));

            Assert.IsNull(task.Name);
        }

        [TestMethod]
        public void TestTaskFullConstructorFailCreator()
        {
            User user = new User("Иванов", "Иван", "Иванович", "Employee", null, 0);
            Task task = new Task(1, "task1", null, 0, "task_description", 2, user, 5, 0, "1", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18));

            Assert.IsNull(task.Name);
        }

        [TestMethod]
        public void TestGetFullInfo()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            User user = new User("Иванов", "Иван", "Иванович", "Employee", manager, 0);
            Task task = new Task(1, "task1", manager, 0, "task_description", 2, user, 5, 0, "active", new DateTime(2019, 11, 14), new DateTime(2019, 11, 15), new DateTime(2019, 12, 18));

            string[] Param = task.GetFullInfo();

            Assert.AreEqual("1", Param[0]);
            Assert.AreEqual("task1", Param[1]);
            Assert.AreEqual("task_description", Param[2]);
            Assert.AreEqual(manager.Id.ToString(), Param[3]);
            Assert.AreEqual("2", Param[4]);
            Assert.AreEqual(user.Id.ToString(), Param[5]);
            Assert.AreEqual("5", Param[6]);
            Assert.AreEqual("active", Param[7]);
            Assert.IsNotNull(Param[7]);
            Assert.AreEqual(new DateTime(2019, 11, 14).ToString(), Param[9]);
            Assert.AreEqual(new DateTime(2019, 11, 15).ToString(), Param[10]);
            Assert.AreEqual(new DateTime(2019, 12, 18).ToString(), Param[11]);
            Assert.IsNull(Param[12]);
            Assert.AreEqual("0", Param[13]);
            Assert.AreEqual("0", Param[14]);
        }

        [TestMethod]
        public void TestAssign()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            User user = new User("Иванов", "Иван", "Иванович", "Employee", manager, 0);
            Task task = new Task(1, "task1", manager, 0);
            task.Assign(user);
            Assert.AreEqual(user, task.Executor);
        }

        [TestMethod]
        public void TestCloseDdtm()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            Task task = new Task(1, "task1", manager, 0);
            Thread.Sleep(10);
            task.Close();
            Assert.IsNotNull(task.Close_dttm);
        }

        [TestMethod]
        public void TestCloseDdtmFail()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            Task task = new Task(1, "task1", manager, 0);
            Thread.Sleep(10);
            task.Close();
            Assert.AreEqual(1, task.Close());
        }

        [TestMethod]
        public void TestIsClosed()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            Task task = new Task(1, "task1", manager, 0);
            Assert.AreEqual(false, task.isClosed());
            Thread.Sleep(10);
            task.Close();
            Assert.AreEqual(true, task.isClosed());
        }

        [TestMethod]
        public void TestIsCompleted()
        {
            User manager = new User("Петров", "Петр", "Петрович", "Manager", null);
            Task task = new Task(1, "task1", manager, 0);
            Assert.AreEqual(false, task.isCompleted());
            task.Progress = 100;
            Assert.AreEqual(true, task.isCompleted());
        }

        [TestMethod]
        public void TestEmptyCpnstructors()
        {
            Task task = new Task();
            TaskInfo taskinfo = new TaskInfo();
            User user = new User();
            UserInfo userinfo = new UserInfo();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [TestMethod]
        public void ConnectDBTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            Assert.IsNotNull(Api.database);
            Api.ConnectDatabase(false);
        }

        [TestMethod]
        public void ConnectRegistSystemTest()
        {
            API Api = new API();
            Api.ConnectRegistrationDatabase(true);

            Assert.IsNotNull(Api.registr);
        }

        [TestMethod]
        public void AuthorizateTest()
        {
            API Api = new API();
            Api.ConnectRegistrationDatabase(true);

            int id = Api.Authorizate("petrov", "pet123");
            Assert.AreEqual(id, 1);
        }

        [TestMethod]
        public void AuthorizateTestFail()
        {
            API Api = new API();
            Api.ConnectRegistrationDatabase(true);

            int id = Api.Authorizate("petrov", "12345");
            Assert.AreEqual(id, -1);
        }

        [TestMethod]
        public void GetUserInfoTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            UserInfo userInfo = Api.GetUserInfoById(2);
            Assert.AreEqual(userInfo.FIO, "Иванов Иван Иванович");
            Assert.AreEqual(userInfo.Role, "Employee");
            Assert.AreEqual(userInfo.ManagerId, 1);
        }

        [TestMethod]
        public void GetSubusersInfoTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            List<UserInfo> users = Api.GetSubusersInfo(1);
            Assert.AreEqual(users.Count, 2);
            Assert.AreEqual(users[0].Id, 2);
            Assert.AreEqual(users[1].Id, 3);
        }

        [TestMethod]
        public void GetSubusersInfoTestFail()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            List<UserInfo> users = Api.GetSubusersInfo(2);
            Assert.IsNull(users);
        }

        [TestMethod]
        public void GetUserTasksInfoTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            List<TaskInfo> tasks = Api.GetUserTasksInfo(1);
            Assert.AreEqual(tasks.Count, 2);
            Assert.AreEqual(tasks[0].TaskNumber, 1);
            Assert.AreEqual(tasks[1].TaskNumber, 3);
        }

        [TestMethod]
        public void GetUserTasksInfoTestFail()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            List<TaskInfo> tasks = Api.GetUserTasksInfo(10);
            Assert.IsNull(tasks);
        }


        [TestMethod]
        public void GetSubusersTasksInfoTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            Dictionary<int, List<TaskInfo>> dict = Api.GetSubuserTasksInfo(1);
            Assert.AreEqual(dict.Count, 2);

            IDatabaseController db = new CurDatabaseController();

            foreach (int userId in dict.Keys)
            {
                Assert.AreEqual(db.GetUserTasks(userId).Count, dict[userId].Count);
                for (int i = 0; i < dict[userId].Count; i++)
                {
                    Assert.AreEqual(db.GetUserTasks(userId)[i].TaskNumber, dict[userId][i].TaskNumber);
                }
            }
        }

        [TestMethod]
        public void GetSubusersTasksInfoTestFail()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            Dictionary<int, List<TaskInfo>> dict = Api.GetSubuserTasksInfo(2);
            Assert.IsNull(dict);
        }

        [TestMethod]
        public void CreateTaskShortTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;
            Assert.IsNull(Api.database.GetTaskByNum(NewNumber));

            int Result = Api.CreateTask("task1", 1);
            Assert.AreEqual(Result, 0);

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
        }       

        [TestMethod]
        public void CreateTaskTestFailNotManager()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;
            Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

            int Result = Api.CreateTask("task1", 2);
            Assert.AreEqual(Result, -1);

            Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        }

        [TestMethod]
        public void CreateTaskTestFailBadExecutor()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            IDatabaseController db = new CurDatabaseController();

            int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

            int Result = Api.CreateTask("task1", 1, "task_description", 2, 10, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
                new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -4);

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        }

        [TestMethod]
        public void CreateTaskTestFailException()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            IDatabaseController db = new CurDatabaseController();

            int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

            int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 15).ToString(), "bad_date",
                new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -6);

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        }

        [TestMethod]
        public void CreateTaskTestFailConstructorName()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            IDatabaseController db = new CurDatabaseController();

            int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

            int Result = Api.CreateTask("", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
                new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -7);

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        }

        [TestMethod]
        public void CreateTaskTestFailConstructorDate()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            IDatabaseController db = new CurDatabaseController();

            int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

            int Result = Api.CreateTask("test", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 11, 15).ToString(),
                new DateTime(2018, 12, 18).ToString());

            Assert.AreEqual(Result, -7);

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        }

        [TestMethod]
        public void CreateTaskTestFailConstructorDate2()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            IDatabaseController db = new CurDatabaseController();

            int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

            int Result = Api.CreateTask("test", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 11, 15).ToString(),
                System.Data.SqlTypes.SqlDateTime.MinValue.Value.ToString());

            Assert.AreEqual(Result, 0);

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        }

        [TestMethod]
        public void CreateTaskTestFailConstructorBadStatus()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            IDatabaseController db = new CurDatabaseController();

            int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

            int Result = Api.CreateTask("test", 1, "task_description", 2, 1, 5, 0, "bad_status", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
                new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -7);

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        }

        [TestMethod]
        public void CreateTaskTestFailConstructorBadProgress()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            IDatabaseController db = new CurDatabaseController();

            int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

            int Result = Api.CreateTask("test", 1, "task_description", 2, 1, 5, -1, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
                new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -7);

            Result = Api.CreateTask("test", 1, "task_description", 2, 1, 5, 101, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
                new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -7);

            Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        }

        [TestMethod]
        public void CreateTaskTestFailAdding_NotUsed()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int Result = Api.database.AddTask(new Task(1, "1", new User(), 1));
            Assert.AreEqual(Result, 1);
        }

        [TestMethod]
        public void GenerateTaskNumberNull()
        {
            Registration db = new Registration(false);

            int Result = db.GenerateNewId();
            Assert.AreEqual(Result, 0);
        }

        [TestMethod]
        public void GenerateIdNull()
        {
            DatabaseController db = new DatabaseController(false);

            int Result = db.GenerateTaskNum();
            Assert.AreEqual(Result, 0);
        }

        [TestMethod]
        public void EditTaskShortTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

            Api.CreateTask("task22", 1);

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

            Api.EditTask(1, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", "", "", "");

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Description, "task_description");
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Priority, 2);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Executor.Id, 1);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Apprasial, 5);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Progress, 0);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Status, "active");
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Assign_dttm, System.Data.SqlTypes.SqlDateTime.MinValue.Value);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_start_dt, System.Data.SqlTypes.SqlDateTime.MinValue.Value);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_close_dt, System.Data.SqlTypes.SqlDateTime.MinValue.Value);
        }

        [TestMethod]
        public void EditTaskTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

            Api.CreateTask("task22", 1);

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

            Api.EditTask(1, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Description, "task_description");
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Priority, 2);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Executor.Id, 1);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Apprasial, 5);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Progress, 0);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Status, "active");
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Assign_dttm, new DateTime(2019, 11, 14));
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_start_dt, new DateTime(2019, 11, 15));
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_close_dt, new DateTime(2019, 12, 18));
        }

        [TestMethod]
        public void EditTaskTestFailBadExecutor()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            IDatabaseController db = new CurDatabaseController();

            int Result = Api.EditTask(1, 1, "task1", "task_description", 2, 10, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -4);
        }

        [TestMethod]
        public void EditTaskTestFailNullTask()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

            int Result = Api.EditTask(0, -1, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString());
            Assert.AreEqual(Result, 1);
        }

        [TestMethod]
        public void EditTaskTestFailINotCreator()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

            Api.CreateTask("task22", 1);

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

            int Result = Api.EditTask(2, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -1);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        }

        [TestMethod]
        public void EditTaskTestFailINotExecutor()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

            Api.CreateTask("task22", 1, "", 1, 1);

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

            int Result = Api.EditTask(2, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -1);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        }

        //[TestMethod]
        //public void EditTaskTestFailBadCheck()
        //{
        //    API Api = new API();
        //    Api.ConnectDatabase(true);

        //    int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //    Api.CreateTask("task22", 1);

        //    Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

        //    int Result = Api.EditTask(1, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString());

        //    Assert.AreEqual(Result, -2);
        //    Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        //}

        [TestMethod]
        public void EditTaskTestFailException()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

            Api.CreateTask("task22", 1);

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

            int Result = Api.EditTask(1, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", "bad_date", new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, -6);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        }

        [TestMethod]
        public void EditTaskTestFailBadConstruct()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

            Api.CreateTask("task22", 1);

            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

            int Result = Api.EditTask(1, NewNumber, "", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString());

            Assert.AreEqual(Result, 1);
            Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        }

        [TestMethod]
        public void RegistrTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);
            Api.ConnectRegistrationDatabase(true);

            int id = Api.Registration("new_login", "password", "Гаврилов", "Михаил", "Иванович", "Manager", 2);

            User user = Api.database.GetUserById(id);

            Assert.AreEqual(user.Id, id);
            Assert.AreEqual(user.Last_name, "Гаврилов");
            Assert.AreEqual(user.First_name, "Михаил");
            Assert.AreEqual(user.Middle_name, "Иванович");
            Assert.AreEqual(user.Role, "Manager");
            Assert.AreEqual(user.Manager.Id, 2);
        }

        [TestMethod]
        public void RegistrTestFail()
        {
            API Api = new API();
            Api.ConnectDatabase(true);
            Api.ConnectRegistrationDatabase(true);

            Api.registr = new Registration(false);
            int id = Api.Registration("new_login", "password", "Гаврилов", "Михаил", "Иванович", "Manager", null);

            Assert.AreEqual(id, -1);
        }

        [TestMethod]
        public void RegistrTestFailLogin()
        {
            API Api = new API();
            Api.ConnectDatabase(true);
            Api.ConnectRegistrationDatabase(true);

            int id = Api.Registration("ivanov", "password", "Гаврилов", "Михаил", "Иванович", "Manager", 1);

            Assert.AreEqual(id, -1);
            Assert.IsNull(Api.database.GetUsers().FirstOrDefault(a => a.Last_name == "Гаврилов"));
        }

        [TestMethod]
        public void RegistrTestNullManager()
        {
            API Api = new API();
            Api.ConnectDatabase(true);
            Api.ConnectRegistrationDatabase(true);

            int id = Api.Registration("new_login", "password", "Гаврилов", "Михаил", "Иванович", "Manager", null);

            User user = Api.database.GetUserById(id);

            Assert.AreEqual(user.Id, id);
            Assert.AreEqual(user.Last_name, "Гаврилов");
            Assert.AreEqual(user.First_name, "Михаил");
            Assert.AreEqual(user.Middle_name, "Иванович");
            Assert.AreEqual(user.Role, "Manager");
            Assert.IsNull(user.Manager);
        }

        [TestMethod]
        public void GetTaskByNumTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            TaskInfo task = Api.GetTaskInfoByNum(1);
            Assert.AreEqual(task.Name, Api.database.GetTaskByNum(1).Name);
            Assert.AreEqual(task.CreatorId, Api.database.GetTaskByNum(1).Creator.Id);
            Assert.AreEqual(task.Description, Api.database.GetTaskByNum(1).Description);
            Assert.AreEqual(task.Priority, Api.database.GetTaskByNum(1).Priority);
            Assert.AreEqual(task.ExecutorId, Api.database.GetTaskByNum(1).Executor.Id);
            Assert.AreEqual(task.Apprasial, Api.database.GetTaskByNum(1).Apprasial);
            Assert.AreEqual(task.Progress, Api.database.GetTaskByNum(1).Progress);
            Assert.AreEqual(task.Status, Api.database.GetTaskByNum(1).Status);
            Assert.AreEqual(task.Assign_dttm, Api.database.GetTaskByNum(1).Assign_dttm.ToString());
            Assert.AreEqual(task.Create_dttm, Api.database.GetTaskByNum(1).Create_dttm.ToString());
            Assert.AreEqual(task.Planned_start_dt, Api.database.GetTaskByNum(1).Planned_start_dt.ToString());
            Assert.AreEqual(task.Planned_close_dt, Api.database.GetTaskByNum(1).Planned_close_dt.ToString());
        }

        [TestMethod]
        public void GetTaskByNumNullTest()
        {
            DatabaseController bd = new DatabaseController();

            Task task = bd.GetTaskByNum(null);
            Assert.IsNull(task);
        }

        [TestMethod]
        public void AddUserFail()
        {
            DatabaseController bd = new DatabaseController();

            int Result = bd.AddUser(new User("", "", "", "", null, 0));
            Assert.AreEqual(Result, 1);
        }

        [TestMethod]
        public void CloseTaskByNumTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            Thread.Sleep(10);

            Api.CloseTask(1);

            Assert.IsTrue(Api.database.GetTaskByNum(1).isClosed());
            Assert.AreNotEqual(Api.GetTaskInfoByNum(1).Close_dttm, "");
        }

        [TestMethod]
        public void GetTaskByNumTestFail()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            TaskInfo task = Api.GetTaskInfoByNum(10);
            Assert.IsNull(task);
        }

        [TestMethod]
        public void GetUserByIdTestFail()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            UserInfo user = Api.GetUserInfoById(10);
            Assert.IsNull(user);
        }

        [TestMethod]
        public void GetUsersTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            List<UserInfo> users = Api.GetUsersInfo();
            Assert.AreEqual(users.Count, 5);
        }

        [TestMethod]
        public void GetTasksTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            List<TaskInfo> tasks = Api.GetTasksInfo();
            Assert.AreEqual(tasks.Count, 4);
        }

        [TestMethod]
        public void SwapTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);

            List<TaskInfo> tasks = Api.GetUserTasksInfo(1);
            Assert.AreEqual(tasks[0].Above, 0);
            Assert.AreEqual(tasks[1].Above, 2);
            Api.SwapTasks(tasks[0].Above, tasks[1].Above);
            tasks = Api.GetUserTasksInfo(1);
            Assert.AreEqual(tasks[0].Above, 2);
            Assert.AreEqual(tasks[1].Above, 0);
        }
    }
}
