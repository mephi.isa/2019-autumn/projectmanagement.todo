﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ToDoListNamespace;
using CurDatabaseController = ToDoListNamespace.SqlDatabaseController;

namespace UnitTestProject
{
    [TestClass]
    public class Tests
    {
        //    [TestMethod]
        //    public void ConnectDBTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Assert.IsNotNull(Api.database);
        //    }

        [TestMethod]
        public void ConnectRegistSystemTest()
        {
            API Api = new API();
            Api.ConnectRegistrationDatabase();

            Assert.IsNotNull(Api.registr);
        }

        [TestMethod]
        public void AuthorizateTest()
        {
            API Api = new API();
            Api.ConnectRegistrationDatabase();

            int id = Api.Authorizate("petrov", "pet123");
            Assert.AreEqual(id, 1);
        }

        [TestMethod]
        public void AuthorizateTestFail()
        {
            API Api = new API();
            Api.ConnectRegistrationDatabase();

            int id = Api.Authorizate("petrov", "12345");
            Assert.AreEqual(id, -1);
        }

        //    [TestMethod]
        //    public void GetUserInfoTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        UserInfo userInfo = Api.GetUserInfoById(2);
        //        Assert.AreEqual(userInfo.FIO, "Иванов Иван Иванович");
        //        Assert.AreEqual(userInfo.Role, "Employee");
        //        Assert.AreEqual(userInfo.ManagerId, 1);
        //    }

        //    [TestMethod]
        //    public void GetSubusersInfoTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        List<UserInfo> users = Api.GetSubusersInfo(1);
        //        Assert.AreEqual(users.Count, 2);
        //        Assert.AreEqual(users[0].Id, 2);
        //        Assert.AreEqual(users[1].Id, 3);
        //    }

        //    [TestMethod]
        //    public void GetSubusersInfoTestFail()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        List<UserInfo> users = Api.GetSubusersInfo(2);
        //        Assert.IsNull(users);
        //    }

        //    [TestMethod]
        //    public void GetUserTasksInfoTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        List<TaskInfo> tasks = Api.GetUserTasksInfo(1);
        //        Assert.AreEqual(tasks.Count, 2);
        //        Assert.AreEqual(tasks[0].TaskNumber, 1);
        //        Assert.AreEqual(tasks[1].TaskNumber, 3);
        //    }

        //    [TestMethod]
        //    public void GetUserTasksInfoTestFail()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        List<TaskInfo> tasks = Api.GetUserTasksInfo(10);
        //        Assert.IsNull(tasks);
        //    }


        //    [TestMethod]
        //    public void GetSubusersTasksInfoTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Dictionary<int, List<TaskInfo>> dict = Api.GetSubuserTasksInfo(1);
        //        Assert.AreEqual(dict.Count, 2);

        //        IDatabaseController db = new CurDatabaseController();

        //        foreach (int userId in dict.Keys)
        //        {
        //            Assert.AreEqual(db.GetUserTasks(userId).Count, dict[userId].Count);
        //            for (int i = 0; i < dict[userId].Count; i++)
        //            {
        //                Assert.AreEqual(db.GetUserTasks(userId)[i].TaskNumber, dict[userId][i].TaskNumber);
        //            }
        //        }
        //    }

        //    [TestMethod]
        //    public void GetSubusersTasksInfoTestFail()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Dictionary<int, List<TaskInfo>> dict = Api.GetSubuserTasksInfo(2);
        //        Assert.IsNull(dict);
        //    }

        //    [TestMethod]
        //    public void CreateTaskShortTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;
        //        Assert.IsNull(Api.database.GetTaskByNum(NewNumber));

        //        int Result = Api.CreateTask("task1", 1);
        //        Assert.AreEqual(Result, 0);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
        //    }

        //    [TestMethod]
        //    public void CreateTaskSimpleTestInsertBetween()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Task above = Api.database.GetTaskByNum(3);
        //        Task below = Api.database.GetTaskByNum(1);

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, 0);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Description, "task_description");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Priority, 2);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Executor.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Apprasial, 5);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Progress, 0);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Status, "active");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Assign_dttm, new DateTime(2019, 11, 14));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_start_dt, new DateTime(2019, 11, 15));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_close_dt, new DateTime(2019, 12, 18));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_above.TaskNumber, above.TaskNumber);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_below.TaskNumber, below.TaskNumber);
        //    }

        //    [TestMethod]
        //    public void CreateTaskSimpleTestInsertBetweenAboveNull()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Task above = Api.database.GetTaskByNum(3);
        //        Task below = Api.database.GetTaskByNum(1);

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), null, below.TaskNumber);

        //        Assert.AreEqual(Result, 0);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Description, "task_description");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Priority, 2);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Executor.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Apprasial, 5);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Progress, 0);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Status, "active");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Assign_dttm, new DateTime(2019, 11, 14));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_start_dt, new DateTime(2019, 11, 15));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_close_dt, new DateTime(2019, 12, 18));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_above.TaskNumber, above.TaskNumber);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_below.TaskNumber, below.TaskNumber);
        //    }

        //    [TestMethod]
        //    public void CreateTaskSimpleTestInsertBetweenBelowNull()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Task above = Api.database.GetTaskByNum(3);
        //        Task below = Api.database.GetTaskByNum(1);

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, null);

        //        Assert.AreEqual(Result, 0);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Description, "task_description");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Priority, 2);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Executor.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Apprasial, 5);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Progress, 0);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Status, "active");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Assign_dttm, new DateTime(2019, 11, 14));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_start_dt, new DateTime(2019, 11, 15));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_close_dt, new DateTime(2019, 12, 18));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_above.TaskNumber, above.TaskNumber);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_below.TaskNumber, below.TaskNumber);
        //    }

        //    [TestMethod]
        //    public void CreateTaskSimpleTestInsertInStart()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Task below = Api.database.GetTaskByNum(3);

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), null, below.TaskNumber);

        //        Assert.AreEqual(Result, 0);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Description, "task_description");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Priority, 2);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Executor.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Apprasial, 5);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Progress, 0);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Status, "active");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Assign_dttm, new DateTime(2019, 11, 14));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_start_dt, new DateTime(2019, 11, 15));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_close_dt, new DateTime(2019, 12, 18));
        //        Assert.IsNull(Api.database.GetTaskByNum(NewNumber).Task_above);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_below.TaskNumber, below.TaskNumber);
        //    }

        //    [TestMethod]
        //    public void CreateTaskSimpleTestInsertInEnd()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Task above = Api.database.GetTaskByNum(1);

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, null);

        //        Assert.AreEqual(Result, 0);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Description, "task_description");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Priority, 2);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Executor.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Apprasial, 5);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Progress, 0);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Status, "active");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Assign_dttm, new DateTime(2019, 11, 14));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_start_dt, new DateTime(2019, 11, 15));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_close_dt, new DateTime(2019, 12, 18));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_above.TaskNumber, above.TaskNumber);
        //        Assert.IsNull(Api.database.GetTaskByNum(NewNumber).Task_below);
        //    }

        //    [TestMethod]
        //    public void CreateTaskSimpleTestInsertBetweenFail()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Task above = Api.database.GetTaskByNum(1);
        //        Task below = Api.database.GetTaskByNum(3);

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, 2);
        //    }

        //    [TestMethod]
        //    public void CreateTaskTestFailNotManager()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;
        //        Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 2);
        //        Assert.AreEqual(Result, -1);

        //        Assert.IsNull(Api.database.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        //    }

        //    [TestMethod]
        //    public void CreateTaskTestFailBadTaskAbove()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        IDatabaseController db = new CurDatabaseController();

        //        Task above = new Task(10, "bad_task", db.GetUserById(0));
        //        Task below = db.GetTaskByNum(3);

        //        int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, -2);

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        //    }

        //    [TestMethod]
        //    public void CreateTaskTestFailBadTaskBelow()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        IDatabaseController db = new CurDatabaseController();

        //        Task above = db.GetTaskByNum(1);
        //        Task below = new Task(10, "bad_task", db.GetUserById(0));

        //        int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, -3);

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        //    }

        //    [TestMethod]
        //    public void CreateTaskTestFailNotMyTaskAbove()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        IDatabaseController db = new CurDatabaseController();

        //        Task above = db.GetTaskByNum(2);
        //        Task below = db.GetTaskByNum(3);

        //        int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, -5);

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        //    }

        //    [TestMethod]
        //    public void CreateTaskTestFailNotMyTaskBelow()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        IDatabaseController db = new CurDatabaseController();

        //        Task above = db.GetTaskByNum(1);
        //        Task below = db.GetTaskByNum(2);

        //        int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, -5);

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        //    }

        //    [TestMethod]
        //    public void CreateTaskTestFailBadExecutor()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        IDatabaseController db = new CurDatabaseController();

        //        Task above = db.GetTaskByNum(1);
        //        Task below = db.GetTaskByNum(3);

        //        int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 10, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, -4);

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        //    }

        //    [TestMethod]
        //    public void CreateTaskTestFailException()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        IDatabaseController db = new CurDatabaseController();

        //        Task above = db.GetTaskByNum(1);
        //        Task below = db.GetTaskByNum(3);

        //        int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("task1", 1, "task_description", 2, 1, 5, 0, "active", "bad_date", new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, -6);

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        //    }

        //    [TestMethod]
        //    public void CreateTaskTestFailConstructor()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        IDatabaseController db = new CurDatabaseController();

        //        Task above = db.GetTaskByNum(1);
        //        Task below = db.GetTaskByNum(3);

        //        int NewNumber = db.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));

        //        int Result = Api.CreateTask("", 1, "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(),
        //            new DateTime(2019, 12, 18).ToString(), above.TaskNumber, below.TaskNumber);

        //        Assert.AreEqual(Result, -7);

        //        Assert.IsNull(db.LoadTasks().FirstOrDefault(a => a.TaskNumber == NewNumber));
        //    }

        //    [TestMethod]
        //    public void EditTaskTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Api.CreateTask("task22", 1);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

        //        Api.EditTask(1, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString(), 1, 3);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task1");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Creator.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Description, "task_description");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Priority, 2);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Executor.Id, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Apprasial, 5);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Progress, 0);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Status, "active");
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Assign_dttm, new DateTime(2019, 11, 14));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_start_dt, new DateTime(2019, 11, 15));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Planned_close_dt, new DateTime(2019, 12, 18));
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_above.TaskNumber, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Task_below.TaskNumber, 3);
        //    }

        //    [TestMethod]
        //    public void EditTaskTestFailNullTask()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        int Result = Api.EditTask(0, -1, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString(), 1, 3);
        //        Assert.AreEqual(Result, 1);
        //    }

        //    [TestMethod]
        //    public void EditTaskTestFailINotCreator()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Api.CreateTask("task22", 1);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

        //        int Result = Api.EditTask(2, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString(), 1, 3);

        //        Assert.AreEqual(Result, -1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        //    }

        //    [TestMethod]
        //    public void EditTaskTestFailINotExecutor()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Api.CreateTask("task22", 1, "", 1, 1);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

        //        int Result = Api.EditTask(2, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString(), 1, 3);

        //        Assert.AreEqual(Result, -1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        //    }

        //    [TestMethod]
        //    public void EditTaskTestFailBadCheck()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Api.CreateTask("task22", 1);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

        //        int Result = Api.EditTask(1, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString(), 10, 3);

        //        Assert.AreEqual(Result, -2);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        //    }

        //    [TestMethod]
        //    public void EditTaskTestFailException()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Api.CreateTask("task22", 1);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

        //        int Result = Api.EditTask(1, NewNumber, "task1", "task_description", 2, 1, 5, 0, "active", "bad_date", new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString(), 1, 3);

        //        Assert.AreEqual(Result, -6);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        //    }

        //    [TestMethod]
        //    public void EditTaskTestFailBadConstruct()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        int NewNumber = Api.database.LoadTasks().Max(b => b.TaskNumber) + 1;

        //        Api.CreateTask("task22", 1);

        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");

        //        int Result = Api.EditTask(1, NewNumber, "", "task_description", 2, 1, 5, 0, "active", new DateTime(2019, 11, 14).ToString(), new DateTime(2019, 11, 15).ToString(), new DateTime(2019, 12, 18).ToString(), 1, 3);

        //        Assert.AreEqual(Result, 1);
        //        Assert.AreEqual(Api.database.GetTaskByNum(NewNumber).Name, "task22");
        //    }

        [TestMethod]
        public void RegistrTest()
        {
            API Api = new API();
            Api.ConnectDatabase(true);
            Api.ConnectRegistrationDatabase();

            int id = Api.Registration("new_login", "password", "Гаврилов", "Михаил", "Иванович", "Manager", 2);

            User user = Api.database.GetUserById(id);

            Assert.AreEqual(user.Id, id);
            Assert.AreEqual(user.Last_name, "Гаврилов");
            Assert.AreEqual(user.First_name, "Михаил");
            Assert.AreEqual(user.Middle_name, "Иванович");
            Assert.AreEqual(user.Role, "Manager");
            Assert.AreEqual(user.Manager.Id, 2);
        }

        [TestMethod]
        public void RegistrTestFailLogin()
        {
            API Api = new API();
            Api.ConnectDatabase(true);
            Api.ConnectRegistrationDatabase();

            int id = Api.Registration("ivanov", "password", "Гаврилов", "Михаил", "Иванович", "Manager", 1);

            Assert.AreEqual(id, -1);
        }

        [TestMethod]
        public void RegistrTestNullManager()
        {
            API Api = new API();
            Api.ConnectDatabase(true);
            Api.ConnectRegistrationDatabase();

            int id = Api.Registration("new_login", "password", "Гаврилов", "Михаил", "Иванович", "Manager", null);

            User user = Api.database.GetUserById(id);

            Assert.AreEqual(user.Id, id);
            Assert.AreEqual(user.Last_name, "Гаврилов");
            Assert.AreEqual(user.First_name, "Михаил");
            Assert.AreEqual(user.Middle_name, "Иванович");
            Assert.AreEqual(user.Role, "Manager");
            Assert.IsNull(user.Manager);
        }

        //    [TestMethod]
        //    public void GetTaskByNumTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        TaskInfo task = Api.GetTaskInfoByNum(1);
        //        Assert.AreEqual(task.Name, Api.database.GetTaskByNum(1).Name);
        //        Assert.AreEqual(task.CreatorId, Api.database.GetTaskByNum(1).Creator.Id);
        //        Assert.AreEqual(task.Description, Api.database.GetTaskByNum(1).Description);
        //        Assert.AreEqual(task.Priority, Api.database.GetTaskByNum(1).Priority);
        //        Assert.AreEqual(task.ExecutorId, Api.database.GetTaskByNum(1).Executor.Id);
        //        Assert.AreEqual(task.Apprasial, Api.database.GetTaskByNum(1).Apprasial);
        //        Assert.AreEqual(task.Progress, Api.database.GetTaskByNum(1).Progress);
        //        Assert.AreEqual(task.Status, Api.database.GetTaskByNum(1).Status);
        //        Assert.AreEqual(task.Assign_dttm, Api.database.GetTaskByNum(1).Assign_dttm.ToString());
        //        Assert.AreEqual(task.Create_dttm, Api.database.GetTaskByNum(1).Create_dttm.ToString());
        //        Assert.AreEqual(task.Planned_start_dt, Api.database.GetTaskByNum(1).Planned_start_dt.ToString());
        //        Assert.AreEqual(task.Planned_close_dt, Api.database.GetTaskByNum(1).Planned_close_dt.ToString());
        //        Assert.AreEqual(task.Task_aboveNumber, Api.database.GetTaskByNum(1).Task_above.TaskNumber);
        //        Assert.AreEqual(task.Task_belowNumber, -1);
        //    }

        //    [TestMethod]
        //    public void CloseTaskByNumTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        Thread.Sleep(10);

        //        Api.CloseTask(1);

        //        Assert.IsTrue(Api.database.GetTaskByNum(1).isClosed());
        //        Assert.AreNotEqual(Api.GetTaskInfoByNum(1).Close_dttm, "");
        //    }

        //    [TestMethod]
        //    public void GetTaskByNumTestFail()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        TaskInfo task = Api.GetTaskInfoByNum(10);
        //        Assert.IsNull(task);
        //    }

        //    [TestMethod]
        //    public void GetUserByIdTestFail()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        UserInfo user = Api.GetUserInfoById(10);
        //        Assert.IsNull(user);
        //    }

        //    [TestMethod]
        //    public void GetUsersTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        List<UserInfo> users = Api.GetUsersInfo();
        //        Assert.AreEqual(users.Count, 5);
        //    }

        //    [TestMethod]
        //    public void GetTasksTest()
        //    {
        //        API Api = new API();
        //        Api.ConnectDatabase();

        //        List<TaskInfo> tasks = Api.GetTasksInfo();
        //        Assert.AreEqual(tasks.Count, 4);
        //    }
        //}
    }
}