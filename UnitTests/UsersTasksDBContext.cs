﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ToDoListNamespace
{
    public partial class UsersTasksDBContext : DbContext
    {
        bool Test = false;
        public UsersTasksDBContext(bool test = false)
        {
            Test = test;
        }

        public UsersTasksDBContext(DbContextOptions<UsersTasksDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TaskDb> TaskDb { get; set; }
        public virtual DbSet<UserDb> UserDb { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string serverUrl = "Server=DESKTOP-ETA7GBM\\REIDAK;Database=UsersTasksDB;Trusted_Connection=True;";
                if (Test)
                    serverUrl = "Server=DESKTOP-ETA7GBM\\REIDAK;Database=UsersTasksDBTest;Trusted_Connection=True;";

#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(serverUrl);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskDb>(entity =>
            {
                entity.HasKey(e => e.TaskNumber)
                    .HasName("PK_Task");

                entity.ToTable("TaskDB");

                entity.Property(e => e.TaskNumber).ValueGeneratedNever();

                entity.Property(e => e.AssignDttm)
                    .HasColumnName("Assign_dttm")
                    .HasColumnType("datetime");

                entity.Property(e => e.CloseDttm)
                    .HasColumnName("Close_dttm")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreateDttm)
                    .HasColumnName("Create_dttm")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PlannedCloseDt)
                    .HasColumnName("Planned_close_dt")
                    .HasColumnType("datetime");

                entity.Property(e => e.PlannedStartDt)
                    .HasColumnName("Planned_start_dt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.TaskAbove).HasColumnName("Task_above");

                entity.Property(e => e.TaskBelow).HasColumnName("Task_below");

                entity.HasOne(d => d.CreatorNavigation)
                    .WithMany(p => p.TaskDbCreatorNavigation)
                    .HasForeignKey(d => d.Creator)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Creator");

                entity.HasOne(d => d.ExecutorNavigation)
                    .WithMany(p => p.TaskDbExecutorNavigation)
                    .HasForeignKey(d => d.Executor)
                    .HasConstraintName("FK_Executor");

                entity.HasOne(d => d.TaskAboveNavigation)
                    .WithMany(p => p.InverseTaskAboveNavigation)
                    .HasForeignKey(d => d.TaskAbove)
                    .HasConstraintName("FK_Task_above");

                entity.HasOne(d => d.TaskBelowNavigation)
                    .WithMany(p => p.InverseTaskBelowNavigation)
                    .HasForeignKey(d => d.TaskBelow)
                    .HasConstraintName("FK_Task_below");
            });

            modelBuilder.Entity<UserDb>(entity =>
            {
                entity.ToTable("UserDB");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("First_name")
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("Last_name")
                    .HasMaxLength(50);

                entity.Property(e => e.MiddleName)
                    .IsRequired()
                    .HasColumnName("Middle_name")
                    .HasMaxLength(50);

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.ManagerNavigation)
                    .WithMany(p => p.InverseManagerNavigation)
                    .HasForeignKey(d => d.Manager)
                    .HasConstraintName("FK_Manager");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
